# SECL: A Zero-Day Attack Detector and Classifier based on Contrastive Learning and Strong Regularization
This git repository provides code and instructions to reproduce results from [this paper](https://doi.org/10.1145/3664476.3664505 "SECL: A Zero-Day Attack Detector and Classifier based on Contrastive Learning and Strong Regularization").

## Python environment

Both methods will install python packages along with nvidia and CUDA packages to run models on GPU.

Experiments were tested using NVIDIA driver *545.29.02* and CUDA version *12.2*.

### Flake & nix enabled system

Using flake.nix, you only need to do:

```
nix develop
pip install torch pytorch-lightning dvc numpy pytest joblib matplotlib seaborn pyyaml pyqt5 aim
```

### Generic system

It is possible to create a python environment (preferably with Python 3.11) using the method of your choice. Required libraries are available in requirements.txt.

```
pip install -r requirements.txt
```

Or you can use any method of your choice, at your own risk.

## Obtain datasets

All three datasets are publicly available. If needed, the following presents instructions to download them.


### WADI

The dataset might be obtained by filling a form available at https://itrust.sutd.edu.sg/itrust-labs_datasets/


### UNSW-NB15

All four dataset files are available at https://research.unsw.edu.au/projects/unsw-nb15-dataset

### CIC-IDS2017

The full dataset used is the csv dataset and can be downloaded from https://www.unb.ca/cic/datasets/ids-2017.html (once again with a form).

