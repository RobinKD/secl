"""Base class to create datasets.

Will be inherited by other dataset classes.
"""

import json
import logging
import os
import pickle
from pathlib import Path

import numpy as np
import pandas as pd
import torch
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import MinMaxScaler, RobustScaler, StandardScaler
from torch.utils.data import Dataset

from src.my_exps.utils import MyJsonEncoder

logging_format = "%(asctime)s - %(levelname)s - %(dataset_name)s dataset: %(message)s"
logger = logging.getLogger("dataset_logger")
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(logging_format))
logger.addHandler(handler)
logger.setLevel(10)


class UnknownDatasetError(Exception):

    """Raise Exception when dataset is unknown."""

    def __init__(self) -> None:
        """Initialize dataset name error."""
        super().__init__("Unknown dataset name")


class WrongDatasetPathError(Exception):

    """Raise Exception when dataset does not exist."""

    def __init__(self) -> None:
        """Initialize path error."""
        super().__init__("Dataset path incorrect")


class BasicDataset:

    """Basic dataset class.

    Will be inherited by other dataset classes
    """

    dataset_name = None

    def __init__(self, path: str, random_seed: int = 43) -> None:
        """Initiliaze dataset path."""
        self.random_seed = random_seed
        dataset_path = Path.cwd() / path
        if dataset_path.exists():
            self.path = Path.cwd() / path
        else:
            raise WrongDatasetPathError

    def get_data(self) -> None:
        """Load dataset using pandas."""
        filenames = [self.path / f for f in os.listdir(self.path)]
        self.data = pd.concat([pd.read_csv(f) for f in filenames])

    def clean(self) -> None:
        """Clean dataset by removing instances with NaN values."""
        self.data = self.data.replace([np.inf, -np.inf], np.nan)
        logger.info(
            "%s",
            f"Dropping { np.count_nonzero(self.data.isna().sum(axis=1))} instances with NaN",
            extra={"dataset_name": self.dataset_name},
        )
        self.data = self.data.dropna(axis=0).reset_index(drop=True)

        # Find categorical features that have numeric values
        # Features that have a unique value are to be dropped
        self.nb_columns_dropped = 0
        uniq_val_cols = self.data.columns[:-1][(self.data.iloc[:, :-1].nunique() == 1)]
        if len(uniq_val_cols) > 0:
            self.nb_columns_dropped = len(uniq_val_cols)
            self.data = self.data.drop(columns=uniq_val_cols)
            logger.info(
                "%s",
                f"Dropped the following columns having a unique value:\n{uniq_val_cols}",
                extra={"dataset_name": self.dataset_name},
            )

    def drop_labels(self) -> None:
        """Replace string labels by integers and separates labels from data."""
        self.labels_dict = {
            "normal": 0,
            **{
                x: i + 1
                for i, x in enumerate(
                    [a for a in self.data["Label"].unique() if a != "normal"],
                )
            },
        }
        self.labels = self.data["Label"].replace(self.labels_dict)
        self.data = self.data.drop(columns=["Label"], axis=1, inplace=False)
        self.nb_features = len(self.data.columns)
        self.feature_names = self.data.columns
        self.class_support = self.labels.value_counts().to_dict()
        self.nb_classes = len(np.unique(self.labels))

    def onehot_encoding(self, max_same_values: int = 9) -> None:
        """Encode feature values when they have few values."""
        # Features that have few possible values can be onehot encoded
        self.categorical_features = self.data.columns[:-1][
            (self.data.iloc[:, :-1].nunique() <= max_same_values)
            & (self.data.iloc[:, :-1].nunique() > 2)
        ].tolist()
        uniq_vals = {
            col: np.unique(self.data[col]) for col in self.categorical_features
        }
        logger.info(
            "%s",
            "The following features will be onehot encoded,"
            f" because they have less than {max_same_values + 1} unique values:\n"
            f"{uniq_vals}",
            extra={"dataset_name": self.dataset_name},
        )
        self.data = pd.get_dummies(self.data, columns=list(self.categorical_features))
        self.nb_features = len(self.data.columns)
        self.feature_names = self.data.columns

    def keep_part_data(
        self,
        proportion: float = 1.0,
        min_instances_class: int = 10,
    ) -> None:
        """Keep only a proportion of the shuffled dataset, for faster experiments."""
        if proportion < 1 and proportion > 0:
            instances_perclass = {
                label: np.sum(self.labels == label) for label in np.unique(self.labels)
            }
            instances_tokeep = {
                label: max(
                    min(min_instances_class, instances),
                    int(instances * proportion),
                )
                for label, instances in instances_perclass.items()
            }
            new_data = []
            new_labels = []
            sum_instances = sum(instances_tokeep.values())
            for sample, label in zip(self.data.to_numpy(), self.labels, strict=True):
                if instances_tokeep[label] > 0:
                    new_data.append(sample)
                    new_labels.append(label)
                    instances_tokeep[label] -= 1
                    sum_instances -= 1
                if sum_instances == 0:
                    break
            self.data = pd.DataFrame(new_data)
            self.labels = pd.Series(new_labels)
        elif proportion != 1:
            logger.warning(
                "Incorrect proportion value, keeping all data...",
                extra={"dataset_name": self.dataset_name},
            )

    def train_test_split(self, train_prop: float = 0.7) -> None:
        """Separate dataset into train / test.

        Done with proportion according to parameter.
        A bit faster with numpy than pytorch.
        """
        sss = StratifiedShuffleSplit(
            n_splits=1,
            train_size=train_prop,
            random_state=self.random_seed,
        )
        self.train_index, self.test_index = next(sss.split(self.data, self.labels))
        self.train_data = self.data.iloc[self.train_index]
        self.test_data = self.data.iloc[self.test_index]
        self.train_labels = self.labels.iloc[self.train_index]
        self.test_labels = self.labels.iloc[self.test_index]

    def binary_labels(self, stage: str = "both") -> None:
        """Change attack labels to a single one."""
        train_dict = self.get_train_dict()
        test_dict = self.get_test_dict()
        match stage:
            case "train" | "both":
                (
                    self.train_labels[self.train_labels == train_dict["normal"]],
                    self.train_labels[self.train_labels != train_dict["normal"]],
                ) = (0, 1)
                self.train_dict = {"normal": 0, "attack": 1}
            case "test" | "both":
                (
                    self.test_labels[self.test_labels == test_dict["normal"]],
                    self.test_labels[self.test_labels != test_dict["normal"]],
                ) = (0, 1)
                self.test_dict = {"normal": 0, "attack": 1}
            case _:
                logger.warning(
                    "%s",
                    f"Incorrect value ({stage}) for stage parameter.\n"
                    f"Choose either 'train', 'test' or 'both'.",
                    extra={"dataset_name": self.dataset_name},
                )

    def normalize_data(
        self,
        scaler: str | None = "StandardScaler",
        **kwargs: dict,
    ) -> None:
        """Normalize data.

        Uses sklearn scaler classes.
        """
        match scaler:
            case "RobustScaler":
                quantile_range = kwargs.get("quantile_range", (1, 99))
                normalizer = RobustScaler(quantile_range=quantile_range)
            case "StandardScaler":
                normalizer = StandardScaler()
            case "MinMaxScaler":
                normalizer = MinMaxScaler()
            case _:
                logger.warning(
                    "Unknown scaler, default to StandardScaler",
                    extra={"dataset_name": self.dataset_name},
                )
                normalizer = StandardScaler()

        normalizer.fit(self.train_data)
        self.normalizer = normalizer
        self.train_data = pd.DataFrame(
            normalizer.transform(self.train_data),
            columns=normalizer.feature_names_in_,
        )
        self.test_data = pd.DataFrame(
            normalizer.transform(self.test_data),
            columns=normalizer.feature_names_in_,
        )

    def remove_class(
        self,
        dataset: str = "train",
        removed_classes: list[str] | None = None,
        num_random_removed: int = 0,
        unremovable: list[str] | None = None,
    ) -> None:
        """Remove some classes from train, test, or both.

        Can be random with a number of classes to remove or can directly
        specify name of classes to remove
        """
        if removed_classes is None:
            removed_classes = []
        if unremovable is None:
            unremovable = []
        nb_classes = len(np.unique(self.train_labels))
        if (len(removed_classes) == 0 and num_random_removed == 0) or dataset not in [
            "train",
            "both",
        ]:
            error_msg = """Invalid class removal options (missing removed_classes
            and num_random_removed = 0, or dataset_classes_to_remove is incorrect)"""
            raise ValueError(error_msg)
        for c in unremovable:
            if c not in self.labels_dict:
                error_msg = f"Possible classes: {self.labels_dict.keys()}"
                raise ValueError(error_msg)
            if c in removed_classes:
                error_msg = "You are trying to remove an unremovable class"
                raise ValueError(error_msg)

        nb_classes_removed = (
            num_random_removed + len(removed_classes) + len(unremovable)
        )
        if nb_classes_removed > nb_classes:
            error_msg = f"Wants to remove too many classes ({nb_classes_removed}"
            f" > {nb_classes})"
            raise ValueError(error_msg)

        rng = np.random.default_rng(seed=self.random_seed)
        reverse_dict = {i: x for x, i in self.labels_dict.items()}
        available_to_remove = [
            i
            for i in range(nb_classes)
            if (unremovable is None or reverse_dict[i] not in unremovable)
            and (removed_classes is not None and reverse_dict[i] not in removed_classes)
        ]
        will_remove_classes = [self.labels_dict[rc] for rc in removed_classes]
        if num_random_removed > 0:
            will_remove_classes += rng.choice(
                available_to_remove,
                num_random_removed,
                replace=False,
            ).tolist()
        index_kept = [
            i
            for i, val in enumerate(self.train_labels)
            if val not in will_remove_classes
        ]
        self.train_data = self.train_data.iloc[index_kept]
        self.train_labels = self.train_labels.iloc[index_kept]
        new_labels = {
            **{label: i for i, label in enumerate(np.unique(self.train_labels))},
            **{
                label: i + (nb_classes - len(will_remove_classes))
                for i, label in enumerate(will_remove_classes)
            },
        }
        self.train_labels = self.train_labels.replace(new_labels)
        self.labels_dict = {k: new_labels[v] for k, v in self.labels_dict.items()}
        if dataset.lower() == "both":
            index_kept = [
                i
                for i, val in enumerate(self.test_labels)
                if val not in will_remove_classes
            ]
            self.test_data = self.test_data.iloc[index_kept]
            self.test_labels = self.test_labels.iloc[index_kept]
        self.test_labels = self.test_labels.replace(new_labels)
        self.removed_classes = [(reverse_dict[c], dataset) for c in will_remove_classes]
        for c, d in self.removed_classes:
            logger.info(
                "%s",
                f"Removed class {c} in {d} dataset{'s' if d == 'both' else ''}",
                extra={"dataset_name": self.dataset_name},
            )

    def make_dataset(
        self,
        proportion: float = 1,
        min_instances_per_class: int = 10,
        proportion_train: float = 0.7,
        scaler: str | None = None,
        *,
        multi: bool = True,
        binary_stage: str = "both",
        onehot_encode: bool = True,
        removed_classes: list | None = None,
        num_random_removed: int = 0,
        dataset_classes_to_remove: str = "train",
        unremovable: list | None = None,
        **kwargs: dict,
    ) -> None:
        """Actually create the dataset."""
        self.get_data()
        self.clean()
        self.drop_labels()

        if onehot_encode:
            self.onehot_encoding()
        self.keep_part_data(proportion, min_instances_per_class)
        self.train_test_split(proportion_train)
        if scaler is not None:
            self.normalize_data(scaler)
        if removed_classes or num_random_removed > 0:
            self.remove_class(
                dataset_classes_to_remove,
                removed_classes,
                num_random_removed,
                unremovable,
            )
        if not multi:
            self.binary_labels(binary_stage)

    def get_nb_features(self) -> int:
        return self.nb_features

    def get_feature_names(self) -> list[str]:
        return list(self.train_data.columns)

    def get_categorical_feature_names(self) -> list[str]:
        return list(self.categorical_features)

    def get_nb_classes(self) -> int:
        return self.nb_classes

    def get_nb_classes_train(self) -> int:
        return len(np.unique(self.train_labels))

    def get_classes_support(self) -> dict[int, int]:
        return self.class_support

    def get_train_classes_support(self) -> dict[int, int]:
        _, train_labels = self.get_train_dataset()
        return {
            int(k): int(val)
            for k, val in dict(
                zip(*np.unique(train_labels, return_counts=True), strict=True),
            ).items()
        }

    def get_test_classes_support(self) -> dict[int, int]:
        _, test_labels = self.get_test_dataset()
        return {
            int(k): int(val)
            for k, val in dict(
                zip(*np.unique(test_labels, return_counts=True), strict=True),
            ).items()
        }

    def get_labels_dict(self) -> dict[str, int]:
        return self.labels_dict

    def get_removed_classes(self) -> list[tuple[str, str]] | None:
        try:
            return self.removed_classes
        except AttributeError:
            return None

    def get_train_dataset(self) -> tuple[np.ndarray, np.ndarray]:
        return self.train_data.to_numpy(), self.train_labels.to_numpy()

    def get_test_dataset(self) -> tuple[np.ndarray, np.ndarray]:
        return self.test_data.to_numpy(), self.test_labels.to_numpy()

    def get_dataset(self) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        train_data, train_labels = self.get_train_dataset()
        test_data, test_labels = self.get_test_dataset()
        return train_data, train_labels, test_data, test_labels

    def get_normalizer(self):
        return self.normalizer

    def get_train_dict(self) -> dict[str, int]:
        try:
            return self.train_dict
        except AttributeError:
            return {
                x: i
                for x, i in self.get_labels_dict().items()
                if i in np.unique(self.train_labels)
            }

    def get_test_dict(self) -> dict[str, int]:
        try:
            return self.test_dict
        except AttributeError:
            return {
                x: i
                for x, i in self.get_labels_dict().items()
                if i in np.unique(self.test_labels)
            }

    def write_to_file(
        self,
        dataset_name: str,
        *,
        save_normalizer: bool = False,
        save_csv: bool = False,
    ) -> None:
        """Write dataset to files used later in experiments."""
        train_data, train_labels, test_data, test_labels = self.get_dataset()
        Path(f"processed_data/{dataset_name}").mkdir(parents=True, exist_ok=True)
        with Path(f"processed_data/{dataset_name}/train_data.npy").open("wb") as f:
            np.save(f, train_data)
        with Path(f"processed_data/{dataset_name}/train_labels.npy").open("wb") as f:
            np.save(f, train_labels)
        with Path(f"processed_data/{dataset_name}/test_data.npy").open("wb") as f:
            np.save(f, test_data)
        with Path(f"processed_data/{dataset_name}/test_labels.npy").open("wb") as f:
            np.save(f, test_labels)
        with Path(f"processed_data/{dataset_name}/dataset_info.json").open("w") as f:
            dataset_info = {
                "nb_features": self.get_nb_features(),
                "feature_names": self.get_feature_names(),
                "categorical_features": self.get_categorical_feature_names(),
                "nb_classes": self.get_nb_classes(),
                "nb_classes_train": self.get_nb_classes_train(),
                "class_support": self.get_classes_support(),
                "train_support": self.get_train_classes_support(),
                "test_support": self.get_test_classes_support(),
                "labels_dict": self.get_labels_dict(),
                "train_labels_dict": self.get_train_dict(),
                "test_labels_dict": self.get_test_dict(),
            }
            removed_classes = self.get_removed_classes()
            if removed_classes:
                dataset_info = {**dataset_info, "removed_classes": removed_classes}
            logger.info(
                "%s",
                f"Dataset info:\n{dataset_info}",
                extra={"dataset_name": self.dataset_name},
            )
            json.dump(dataset_info, f, cls=MyJsonEncoder)
        if save_normalizer:
            with Path(f"processed_data/{dataset_name}/normalizer.pkl").open(
                "wb",
            ) as f:
                pickle.dump(self.get_normalizer(), f)
        if save_csv:
            feat_names = self.get_feature_names()
            pd_train = pd.DataFrame(
                np.concatenate((train_data, train_labels.reshape(-1, 1)), axis=1),
                columns=[*feat_names, "Labels"],
            )
            pd_train.to_csv(self.path / f"{dataset_name}_train.csv", index=False)
            pd_test = pd.DataFrame(
                np.concatenate((test_data, test_labels.reshape(-1, 1)), axis=1),
                columns=[*feat_names, "Labels"],
            )
            pd_test.to_csv(self.path / f"{dataset_name}_test.csv", index=False)
            pd_info = pd.DataFrame(dataset_info)
            pd_info.to_csv(self.path / f"{dataset_name}_dataset_info.csv", index=False)


class TorchDataset(Dataset):

    """Create a simple class for custom getter and len methods."""

    def __init__(self, data: np.ndarray, labels: np.ndarray) -> None:
        self.data = torch.from_numpy(data).float()
        self.targets = torch.from_numpy(labels).long()

    def __len__(self) -> int:
        return len(self.targets)

    def __getitem__(self, idx):
        return self.data[idx], self.targets[idx]


class TorchDatasetPercentageLabeled(TorchDataset):

    """Create a simple class for custom getter and len methods."""

    def __init__(
        self,
        data: np.ndarray,
        labels: np.ndarray,
        percentage_labeled: float,
    ) -> None:
        self.data = torch.from_numpy(data).float()
        self.targets = torch.from_numpy(labels).long()
        self.unlabeled_targets = self.targets.clone()
        for c in torch.unique(self.unlabeled_targets):
            targets_c = self.unlabeled_targets[self.unlabeled_targets == c]
            perm_idx = torch.randperm(len(targets_c))
            # At least one labeled
            unlabeled_idx = perm_idx[max(1, int(len(perm_idx) * percentage_labeled)) :]
            targets_c[unlabeled_idx] = -1
            self.unlabeled_targets[self.unlabeled_targets == c] = targets_c

    def __len__(self) -> int:
        return len(self.targets)

    def __getitem__(self, idx):
        return self.data[idx], self.targets[idx], self.unlabeled_targets[idx]


class TorchDatasetWithCVSS(TorchDataset):

    """Simple class for custom getter and len methods."""

    def __init__(
        self,
        data: np.ndarray,
        labels: np.ndarray,
        cvss_scores: np.ndarray,
    ) -> None:
        super().__init__(data, labels)
        self.cvss_scores = torch.from_numpy(cvss_scores).float()

    def __getitem__(self, idx):
        return self.data[idx], self.targets[idx], self.cvss_scores[idx]
