import json
from pathlib import Path

import numpy as np
import pandas as pd

from .basic_dataset import BasicDataset


class CICIDS2017Dataset(BasicDataset):

    """Aggregate the CIC-IDS2017 dataset.

    ([from Kaggle](https://www.kaggle.com/cicdataset/cicids2017))
    from files present in a local directory, and perform relevant cleaning
    """

    dataset_name = "CIC-IDS2017"

    def __init__(self, path: str, random_seed: int = 43) -> None:
        """Initiliaze dataset path."""
        super().__init__(path, random_seed)
        self.with_cve = False
        self.cve_envs = False

    def clean(self) -> None:
        """Clean dataset.

        Remove instances with infinite and weird values < 0,
        except for features "Init_Win_bytes_forward", "Init_Win_bytes_backward"
        that contain too many of them
        """
        # Sanitize weird char in web attack labels and remove unnecessary spaces
        self.data = self.data.replace("[^A-Za-z0-9 -]", "", regex=True).replace(
            "[ ]+",
            " ",
            regex=True,
        )
        self.data[
            self.data.drop(
                ["Init_Win_bytes_forward", "Init_Win_bytes_backward", "Label"],
                axis=1,
            )
            < 0
        ] = np.nan
        super().clean()
        if self.with_cve:
            self.add_cvss(cve_envs=self.cve_envs)

    def add_cvss(self, *, cve_envs: bool = False) -> None:
        """Add CVSS scores to the CIC-IDS2017.

        Use https://www.first.org/cvss/calculator/3.1 to do so.
        """
        scores = {
            "BENIGN": -1,
            "FTP-Patator": 5.3,  # N L N N U L N N
            "SSH-Patator": 5.3,  # N L N N U L N N
            "DoS slowloris": 5.3,  # N L N N U N N L
            "DoS Slowhttptest": 5.3,  # N L N N U N N L
            "DoS GoldenEye": 5.3,  # N L N N U N N L
            "DoS Hulk": 5.3,  # N L N N U N N L
            "DDoS": 7.5,  # N L N N U N N H
            "Heartbleed": 7.5,  # N L N N U H N N
            "Web Attack XSS": 5.3,  # N L N N U N L N
            "Web Attack Brute Force": 5.3,  # N L N N U L N N
            "Web Attack Sql Injection": 7.3,  # N L N N U L L L
            "Infiltration": 4.7,  # L H N R U H N N
            "PortScan": 5.3,  # N L N N U L N N
            "Bot": 9.8,  # N L N N U H H H
        }

        # Taking into account environmental scores
        env_scores = [
            {
                # High conf need, low integ need, and low avail need
                "BENIGN": -1,
                "FTP-Patator": 6.1,  # N L N N U L N N
                "SSH-Patator": 6.1,  # N L N N U L N N
                "DoS slowloris": 4.6,  # N L N N U N N L
                "DoS Slowhttptest": 4.6,  # N L N N U N N L
                "DoS GoldenEye": 4.6,  # N L N N U N N L
                "DoS Hulk": 4.6,  # N L N N U N N L
                "DDoS": 5.7,  # N L N N U N N H
                "Heartbleed": 9.3,  # N L N N U H N N
                "Web Attack XSS": 4.6,  # N L N N U N L N
                "Web Attack Brute Force": 6.1,  # N L N N U L N N
                "Web Attack Sql Injection": 6.9,  # N L N N U L L L
                "Infiltration": 6.5,  # L H N R U H N N
                "PortScan": 6.1,  # N L N N U L N N
                "Bot": 9.8,  # N L N N U H H H
            },
            # Taking into account environmental scores
            {
                # High avail need, low integ need, and low conf need
                "BENIGN": -1,
                "FTP-Patator": 4.6,  # N L N N U L N N
                "SSH-Patator": 4.6,  # N L N N U L N N
                "DoS slowloris": 6.1,  # N L N N U N N L
                "DoS Slowhttptest": 6.1,  # N L N N U N N L
                "DoS GoldenEye": 6.1,  # N L N N U N N L
                "DoS Hulk": 6.1,  # N L N N U N N L
                "DDoS": 9.3,  # N L N N U N N H
                "Heartbleed": 5.7,  # N L N N U H N N
                "Web Attack XSS": 4.6,  # N L N N U N L N
                "Web Attack Brute Force": 4.6,  # N L N N U L N N
                "Web Attack Sql Injection": 6.9,  # N L N N U L L L
                "Infiltration": 2.9,  # L H N R U H N N
                "PortScan": 4.6,  # N L N N U L N N
                "Bot": 9.8,  # N L N N U H H H
            },
        ]
        self.cvss_scores = pd.Series(
            [[scores[lab]] if lab != "BENIGN" else -1 for lab in self.data["Label"]],
        )
        if cve_envs:
            self.additional_cvss_scores = [
                pd.Series(
                    [[s[lab]] if lab != "BENIGN" else -1 for lab in self.data["Label"]],
                )
                for s in env_scores
            ]

    def drop_labels(self) -> None:
        """Replace string labels by integers and separates labels from data."""
        self.data.loc[self.data["Label"] == "BENIGN", "Label"] = "normal"
        super().drop_labels()

    def train_test_split(
        self,
        train_prop: float = 0.7,
    ) -> None:
        """Overload parent method by adding environmental cvss scores if needed."""
        super().train_test_split(train_prop)
        if self.with_cve:
            self.train_cvss_scores = self.cvss_scores.iloc[self.train_index]
            self.test_cvss_scores = self.cvss_scores.iloc[self.test_index]

        if self.cve_envs:
            self.additional_test_cvss_scores = [
                cvss.iloc[self.test_index] for cvss in self.additional_cvss_scores
            ]

    def make_dataset(
        self,
        proportion: float = 1,
        min_instances_per_class: int = 10,
        proportion_train: float = 0.7,
        scaler: str | None = None,
        *,
        multi: bool = True,
        onehot_encode: bool = True,
        removed_classes: list | None = None,
        num_random_removed: int = 0,
        dataset_classes_to_remove: str = "train",
        unremovable: list | None = None,
        with_cve: bool = False,
        cve_envs: bool = False,
        **kwargs: dict,
    ) -> None:
        """Override parent method."""
        self.with_cve = with_cve
        self.cve_envs = cve_envs
        super().make_dataset(
            proportion,
            min_instances_per_class,
            proportion_train,
            scaler,
            multi=multi,
            onehot_encode=onehot_encode,
            removed_classes=removed_classes,
            num_random_removed=num_random_removed,
            dataset_classes_to_remove=dataset_classes_to_remove,
            unremovable=unremovable,
        )

    def get_cvss_scores(self) -> tuple[np.ndarray, np.ndarray] | tuple[None, None]:
        try:
            return self.train_cvss_scores.to_numpy(), self.test_cvss_scores.to_numpy()
        except AttributeError:
            return None, None

    def write_to_file(
        self,
        dataset_name: str,
        *,
        save_normalizer: bool = False,
        save_csv: bool = False,
    ) -> None:
        """Overload parent method for additional CVSS information."""
        super().write_to_file(
            dataset_name,
            save_normalizer=save_normalizer,
            save_csv=save_csv,
        )
        if self.with_cve:
            train_cvss, test_cvss = self.get_cvss_scores()
            with Path(f"processed_data/{dataset_name}/dataset_info.json").open(
                "r",
            ) as f:
                dataset_info = json.load(f)
                dataset_info = {**dataset_info, "with_cve": True}
            with Path(f"processed_data/{dataset_name}/dataset_info.json").open(
                "w",
            ) as f:
                json.dump(dataset_info, f)
            with Path(f"processed_data/{dataset_name}/train_cvss.npy").open("wb") as f:
                np.save(f, train_cvss)
            with Path(f"processed_data/{dataset_name}/test_cvss.npy").open("wb") as f:
                np.save(f, test_cvss)
            try:
                for i in range(len(self.additional_test_cvss_scores)):
                    with Path(
                        f"processed_data/{dataset_name}/test_cvss_{i + 1}.json",
                    ).open(
                        "w",
                    ) as f:
                        json.dump(self.additional_test_cvss_scores[i], f)
            except AttributeError:
                pass
