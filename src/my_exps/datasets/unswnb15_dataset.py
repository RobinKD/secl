import json
import logging
from pathlib import Path

import numpy as np
import pandas as pd

from .basic_dataset import BasicDataset

logger = logging.getLogger("dataset_logger")
logger.setLevel(10)


class UNSWNB15Dataset(BasicDataset):

    """Aggregate the UNSW-NB15 dataset.

    Files are taken from https://research.unsw.edu.au/projects/unsw-nb15-dataset
    """

    dataset_name = "UNSW-NB15"

    def __init__(self, path: str, random_seed: int = 43) -> None:
        """Initiliaze dataset path."""
        super().__init__(path, random_seed)
        self.with_cve = False
        self.subcats = False

    def get_data(self) -> None:
        """Override parent method."""
        self.feature_names = list(
            pd.read_csv(
                self.path / "UNSW-NB15_features.csv",
                encoding="latin_1",
            )["Name"],
        )
        datafile_1 = pd.read_csv(
            self.path / "UNSW-NB15_1.csv",
            encoding="latin_1",
            names=self.feature_names,
            low_memory=False,
        )
        datafile_2 = pd.read_csv(
            self.path / "UNSW-NB15_2.csv",
            encoding="latin_1",
            names=self.feature_names,
            low_memory=False,
        )
        datafile_3 = pd.read_csv(
            self.path / "UNSW-NB15_3.csv",
            encoding="latin_1",
            names=self.feature_names,
            low_memory=False,
        )
        datafile_4 = pd.read_csv(
            self.path / "UNSW-NB15_4.csv",
            encoding="latin_1",
            names=self.feature_names,
            low_memory=False,
        )
        self.data = pd.concat(
            [datafile_1, datafile_2, datafile_3, datafile_4],
        )

        self.gt_info_path = self.path / "UNSW-NB15_GT.csv"

    def clean(self) -> None:
        """Override parent method."""
        self.data = self.data.replace([np.inf, -np.inf, "", " "], np.nan)
        self.data[["state"]] = self.data[["state"]].replace(["-", "no"], "no_state")
        self.data[["service"]] = self.data[["service"]].replace("-", "no_service")
        self.data[["sport", "dsport"]] = self.data[["sport", "dsport"]].replace(
            "-",
            np.nan,
        )
        self.data[["sport", "dsport"]] = self.data[["sport", "dsport"]].map(
            lambda x: int(x, 16)
            if isinstance(x, str) and "0x" in x
            else int(x)
            if isinstance(x, str)
            else x,
            na_action="ignore",
        )
        self.data[["ct_ftp_cmd"]] = self.data[["ct_ftp_cmd"]].map(
            lambda x: int(x),
            na_action="ignore",
        )
        self.data[["ct_flw_http_mthd", "is_ftp_login", "ct_ftp_cmd"]] = self.data[
            ["ct_flw_http_mthd", "is_ftp_login", "ct_ftp_cmd"]
        ].replace(np.nan, 0)
        self.data[["is_ftp_login"]] = self.data[["is_ftp_login"]].map(
            lambda x: x if x in [0, 1] else np.nan,
            na_action="ignore",
        )  # Why having 2 and 4 in a binary feature??
        nb_max_ports = 65536
        self.data[self.data[["sport", "dsport"]] > nb_max_ports] = np.nan
        self.data[self.data[["sport", "dsport"]] <= 0] = np.nan

        self.data[["attack_cat"]] = (
            self.data[["attack_cat"]]
            .replace("Backdoors", "Backdoor")
            .map(lambda x: x.strip().lower() if isinstance(x, str) else x)
        )
        self.data[["Label"]] = self.data[["attack_cat"]].replace(np.nan, "normal")

        if self.with_cve:
            self.gt_info = pd.read_csv(self.gt_info_path, encoding="latin-1")
            self.gt_info[["Attack category"]] = (
                self.gt_info[["Attack category"]]
                .replace("Backdoors", "Backdoor")
                .map(lambda x: x.strip().lower() if isinstance(x, str) else x)
            )
            self.gt_info = self.gt_info.rename(
                columns={
                    "Start time": "Stime",
                    "Last time": "Ltime",
                    "Attack category": "attack_cat",
                    "Attack subcategory": "attack_subcat",
                    "Source IP": "srcip",
                    "Source Port": "sport",
                    "Destination IP": "dstip",
                    "Destination Port": "dsport",
                    "Protocol": "proto",
                    "Attack Reference": "attack_ref",
                },
            )
            self.add_cvss()

        self.data = self.data.drop(
            ["srcip", "dstip", "Stime", "Ltime", "attack_cat"],
            axis=1,
        )

        super().clean()
        self.nb_columns_dropped += 5
        if self.with_cve:
            self.cvss_scores = self.data["attack_ref"]
            self.data = self.data.drop(columns=["attack_ref"])

    def set_cvss_params(self, cvss_params) -> None:
        """Set CVSS parameters."""
        self.subcats = True
        self.cvss_params = cvss_params

    def add_cvss(self) -> None:
        """Get CVSS information from files."""
        json_cvss = {}

        def get_cvss(ref):
            if "CVSS" not in ref and "CVE" not in ref:
                return -1
            try:
                ref_split = ref.split("/")
                cvss_idx = ref_split.index("CVSS")
                has_score = ref_split[cvss_idx + 1]
                if has_score[:3] == "%20":
                    has_score = has_score[3:]
                score = has_score.split("%")[0]
                return float(score)
            except Exception:
                cve_code = ref.split("/")[0].split(" ")
                if "CVE" in cve_code[0]:
                    cve_code = cve_code[0] + "-" + cve_code[1]
                    parts = cve_code.split("-")
                    if cve_code not in json_cvss:
                        year = int(parts[1])
                        ind = int(parts[2])
                        try:
                            import json

                            # Downloaded from https://nvd.nist.gov/vuln/data-feeds
                            # Before 2002's cve are in 2002 file
                            with Path(
                                self.path
                                / f"/cves/nvdcve-1.1-{year if year > 2002 else 2002}.json/nvdcve-1.1-{year if year > 2002 else 2002}.json",
                            ).open() as json_file:
                                data = json.load(json_file)
                            items = data["CVE_Items"]
                            start = 0
                            while (
                                items[start]["cve"]["CVE_data_meta"]["ID"] != cve_code
                            ):
                                cve_id = items[start]["cve"]["CVE_data_meta"]["ID"]
                                cve_id_parts = cve_id.split("-")
                                if (
                                    int(cve_id_parts[1]) <= year
                                    or int(cve_id_parts[2][:4]) < ind
                                ):
                                    start += 1
                                elif (
                                    int(cve_id_parts[1]) >= year
                                    and int(cve_id_parts[2][:4]) > ind
                                ):
                                    break
                            json_cvss[cve_code] = data["CVE_Items"][ind]["impact"][
                                "baseMetricV2"
                            ]["cvssV2"]["baseScore"]
                        except Exception:
                            return -1
                    return json_cvss[cve_code]

        self.gt_info["attack_ref"] = self.gt_info["attack_ref"].replace(np.nan, "-")
        self.gt_info["attack_ref"] = self.gt_info["attack_ref"].apply(
            lambda ref: get_cvss(ref),
        )

        self.gt_info = (
            self.gt_info.drop_duplicates(
                subset=[
                    "Stime",
                    "Ltime",
                    "attack_cat",
                    "srcip",
                    "sport",
                    "dstip",
                    "dsport",
                    "proto",
                    "attack_ref",
                ],
            )
            .replace(["-", -1], np.nan)
            .dropna(axis=0)
            .drop(columns=".")
        )

        self.gt_info[["attack_cat"]] = (
            self.gt_info[["attack_cat"]]
            .replace("Backdoors", "Backdoor")
            .applymap(lambda x: x.strip().lower() if isinstance(x, str) else x)
        )

        groupby_colums = [
            "Stime",
            "Ltime",
            "attack_cat",
            "srcip",
            "sport",
            "dstip",
            "dsport",
            "proto",
        ]
        if self.subcats:
            self.gt_info["attack_subcat"] = self.gt_info["attack_subcat"].str.strip()
            self.gt_info["attack_cat_subcat"] = (
                self.gt_info[["attack_cat", "attack_subcat"]]
                .astype(str)
                .apply("-".join, axis=1)
            )
            groupby_colums += ["attack_cat_subcat"]
        df2 = self.gt_info.groupby(groupby_colums, as_index=False)[["attack_ref"]].agg(
            lambda x: x if len(list(set(x))) == 1 else -1,
        )
        df2 = df2.drop(df2[df2["attack_ref"] == -1].index)

        if self.subcats:
            df2["attack_cat_subcat_ref"] = (
                df2[["attack_cat_subcat", "attack_ref"]]
                .astype(str)
                .apply("-".join, axis=1)
            )
            indexes_drop = []
            if "Most frequent" in self.cvss_params:
                mf_number = self.cvss_params["Most frequent"]
                indexes_drop += df2[
                    df2["attack_cat_subcat_ref"].isin(
                        df2["attack_cat_subcat_ref"]
                        .value_counts()[mf_number:]
                        .index.tolist(),
                    )
                ].index.tolist()
            if "Chosen subclasses" in self.cvss_params:
                chosen_subclasses = self.cvss_params["Chosen subclasses"]
                indexes_drop += df2[
                    ~df2["attack_cat_subcat_ref"].isin(chosen_subclasses)
                ].index.tolist()
            indexes_drop = list(set(indexes_drop))
            df2 = df2.drop(index=indexes_drop)

        merge_cols = [
            "Stime",
            "Ltime",
            "attack_cat",
            "srcip",
            "sport",
            "dstip",
            "dsport",
            "proto",
            "attack_ref",
        ]
        if self.subcats:
            merge_cols += ["attack_cat_subcat_ref"]
        self.data = self.data.merge(
            df2[merge_cols],
            on=[
                "Stime",
                "Ltime",
                "attack_cat",
                "srcip",
                "sport",
                "dstip",
                "dsport",
                "proto",
            ],
            how="left",
        )
        self.data.loc[self.data["attack_ref"].isna(), "attack_ref"] = -1
        self.data = self.data.drop(
            self.data[
                (self.data["attack_ref"] == -1) & (self.data["Label"] != "normal")
            ].index,
        )
        if self.subcats:
            self.data[["Label"]] = self.data[["attack_cat_subcat_ref"]].replace(
                np.nan,
                "normal",
            )
            self.data = self.data.drop(columns=["attack_cat_subcat_ref"])

    def onehot_encoding(self, max_same_values: int = 9) -> None:
        """Override parent method."""
        # TODO Handle ports - Can be more precise using IANA classification of ports
        system_ports = (0, 1023)
        registered_ports = (1024, 49151)
        dynamic_ports = (49152, 65536)

        self.data["sport_is_sys_port"] = (
            (self.data["sport"] >= system_ports[0])
            .mul(self.data["sport"] <= system_ports[1])
            .astype(np.uint8)
        )
        self.data["dsport_is_sys_port"] = (
            (self.data["dsport"] >= system_ports[0])
            .mul(self.data["dsport"] <= system_ports[1])
            .astype(np.uint8)
        )
        self.data["sport_is_reg_port"] = (
            (self.data["sport"] >= registered_ports[0])
            .mul(self.data["sport"] <= registered_ports[1])
            .astype(np.uint8)
        )
        self.data["dsport_is_reg_port"] = (
            (self.data["dsport"] >= registered_ports[0])
            .mul(self.data["dsport"] <= registered_ports[1])
            .astype(np.uint8)
        )
        self.data["sport_is_dyn_port"] = (
            (self.data["sport"] >= dynamic_ports[0])
            .mul(self.data["sport"] <= dynamic_ports[1])
            .astype(np.uint8)
        )
        self.data["dsport_is_dyn_port"] = (
            (self.data["dsport"] >= dynamic_ports[0])
            .mul(self.data["dsport"] <= dynamic_ports[1])
            .astype(np.uint8)
        )
        self.data = self.data.drop(["sport", "dsport"], axis=1)
        categorical_features = self.data.select_dtypes(
            exclude=np.number,
        ).columns.tolist()
        logger.info(
            "%s",
            f"The following features will be onehot encoded, because they are"
            f" categorical:\n"
            f"{categorical_features}",
            extra={"dataset_name": self.dataset_name},
        )

        self.data = pd.get_dummies(self.data, columns=categorical_features)
        super().onehot_encoding(max_same_values)
        for cat in [
            *categorical_features,
            "sport_is_sys_port",
            "dsport_is_sys_port",
            "sport_is_reg_port",
            "dsport_is_reg_port",
            "sport_is_dyn_port",
            "dsport_is_dyn_port",
        ]:
            self.categorical_features.append(cat)

    def train_test_split(
        self,
        train_prop: float = 0.7,
    ) -> None:
        """Overload parent method by adding environmental cvss scores if needed."""
        super().train_test_split(train_prop)
        if self.with_cve:
            self.train_cvss_scores = self.cvss_scores.iloc[self.train_index]
            self.test_cvss_scores = self.cvss_scores.iloc[self.test_index]

    def make_dataset(
        self,
        proportion: float = 1,
        min_instances_per_class: int = 10,
        proportion_train: float = 0.7,
        scaler: str | None = None,
        *,
        multi: bool = True,
        onehot_encode: bool = True,
        removed_classes: list | None = None,
        num_random_removed: int = 0,
        dataset_classes_to_remove: str = "train",
        unremovable: list | None = None,
        with_cve: bool = False,
        **kwargs: dict,
    ) -> None:
        """Override parent method."""
        self.with_cve = with_cve
        super().make_dataset(
            proportion,
            min_instances_per_class,
            proportion_train,
            scaler,
            multi=multi,
            onehot_encode=onehot_encode,
            removed_classes=removed_classes,
            num_random_removed=num_random_removed,
            dataset_classes_to_remove=dataset_classes_to_remove,
            unremovable=unremovable,
        )

    def get_cvss_scores(self) -> tuple[np.ndarray, np.ndarray] | tuple[None, None]:
        try:
            return self.train_cvss_scores.apply(
                lambda x: np.mean(x),
            ).to_numpy(), self.test_cvss_scores.apply(lambda x: np.mean(x)).to_numpy()
        except AttributeError:
            return None, None

    def write_to_file(
        self,
        dataset_name: str,
        *,
        save_normalizer: bool = False,
        save_csv: bool = False,
    ) -> None:
        """Overload parent method for additional CVSS information."""
        super().write_to_file(
            dataset_name,
            save_normalizer=save_normalizer,
            save_csv=save_csv,
        )
        if self.with_cve:
            train_cvss, test_cvss = self.get_cvss_scores()
            with Path(f"processed_data/{dataset_name}/dataset_info.json").open(
                "r",
            ) as f:
                dataset_info = json.load(f)
                dataset_info = {**dataset_info, "with_cve": True}
            with Path(f"processed_data/{dataset_name}/dataset_info.json").open(
                "w",
            ) as f:
                json.dump(dataset_info, f)
            with Path(f"processed_data/{dataset_name}/train_cvss.npy").open("wb") as f:
                np.save(f, train_cvss)
            with Path(f"processed_data/{dataset_name}/test_cvss.npy").open("wb") as f:
                np.save(f, test_cvss)
