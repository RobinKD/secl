import pandas as pd

from .basic_dataset import BasicDataset


class WADIDataset(BasicDataset):

    """Load and create WADI Dataset."""

    dataset_name = "WADI"

    def get_data(self) -> None:
        """Load dataset using pandas."""
        self.data_1 = pd.read_csv(self.path / "WADI.A2_19_Nov_2019/WADI_14days_new.csv")
        self.data_2 = pd.read_csv(
            self.path / "WADI.A2_19_Nov_2019/WADI_attackdataLABLE.csv",
            header=1,
        )

    def clean(self) -> None:
        """Clean dataset by removing instances with NaN values."""
        self.data_2.columns = self.data_2.columns.str.strip()
        self.multi_labels()
        self.data_1 = self.data_1.drop(
            columns=[
                "Row",
                "Date",
                "Time",
                "2_LS_001_AL",
                "2_LS_002_AL",
                "2_P_001_STATUS",
                "2_P_002_STATUS",
            ],
        )
        self.data_2 = self.data_2.drop(
            columns=[
                "Row",
                "Date",
                "Time",
                "2_LS_001_AL",
                "2_LS_002_AL",
                "2_P_001_STATUS",
                "2_P_002_STATUS",
            ],
        )
        self.data_2.columns = self.data_1.columns
        self.data = pd.concat([self.data_1, self.data_2])
        super().clean()

    def multi_labels(self) -> None:
        """Add labels according to provided attack timestamps."""
        self.data_1["Label"] = "normal"
        self.data_2.iloc[:, -1] = self.data_2.iloc[:, -1].replace(
            {1: "normal", -1: "attack"},
        )
        attack_ranges = [
            [5103, 6603],  # 1
            [59053, 59643],  # 2
            [60903, 62643],  # 3-4
            [63043, 63893],  # 5
            [70773, 71443],  # 6
            [74900, 75598],  # 7
            [85203, 85783],  # 8
            [108453, 108655],  # 13
            [147303, 147390],  # 9
            [148677, 149483],  # 10
            [149749, 150423],  # 11
            [151143, 151503],  # 12
            [152163, 152739],  # 14
            [163593, 164223],  # 15
        ]
        attacks = [
            "attack_1",
            "attack_2",
            "attack_3-4",
            "attack_5",
            "attack_6",
            "attack_7",
            "attack_8",
            "attack_13",
            "attack_9",
            "attack_10",
            "attack_11",
            "attack_12",
            "attack_14",
            "attack_15",
        ]
        for i, ar in enumerate(attack_ranges):
            self.data_2.iloc[ar[0] : ar[1] + 1, -1] = attacks[i]

        self.data_2.iloc[:, -1] = self.data_2.iloc[:, -1].replace(
            {"attack_11": "attack_8", "attack_12": "attack_8"},
        )
        self.data_2 = self.data_2.rename(columns={self.data_2.columns[-1]: "Label"})
