"""Test model stage for dvc experiments."""

import json
import logging
import pickle
from pathlib import Path

import dvc.api
import numpy as np
from aim import Run
from aim.sdk.base_run import MissingRunError
from make_aim import make_aim
from sklearn.tree import DecisionTreeClassifier

logging_format = "%(asctime)s - %(levelname)s - Model testing: %(message)s"
logger = logging.getLogger("test_model")
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(logging_format))
logger.addHandler(handler)
logger.setLevel(10)


def test_model(params: dict) -> None:
    """Train the current model.

    Load test dataset, trained model, and test the model.
    """
    dataset_name = params["dataset"]["name"]

    with Path(f"processed_data/{dataset_name}/dataset_info.json").open("r") as f:
        dataset_info = json.load(f)
    with Path(f"processed_data/{dataset_name}/test_data.npy").open("rb") as f:
        test_data = np.load(f)
    with Path(f"processed_data/{dataset_name}/test_labels.npy").open("rb") as f:
        test_labels = np.load(f)

    test_cvss = None
    if params["dataset"]["parameters"]["with_cve"]:
        with Path(f"processed_data/{dataset_name}/test_cvss.npy").open("rb") as f:
            test_cvss = np.load(f)

    try:
        with Path("current_aim_run").open("r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
    except MissingRunError:
        make_aim(params)
        with Path("current_aim_run").open("r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
    logger.info("%s", f"Current run hash: {aim_runner.hash}")

    with Path("untrained_model.pkl").open("rb") as f:
        model = pickle.load(f)

    Path("save_experiments/test").mkdir(exist_ok=True)
    model.load_model("save_experiments/trained_model")
    args = {
        "x": test_data,
        "y": test_labels,
        "label_dict": dataset_info["test_labels_dict"],
        "aim_runner": aim_runner,
        "image_format": "svg",
        "out_path": Path(f"~/experiments/saved_results/{aim_runner.hash}").expanduser(),
    }
    if test_cvss:
        args = {**args, "test_cvss": test_cvss}
    if isinstance(model.model, DecisionTreeClassifier):
        args = {**args, "feature_names": dataset_info["feature_names"]}

    model.test(**args)

    if (
        params["dataset"]["parameters"].get("cve_envs", False)
        and params["dataset"]["parameters"]["with_cve"]
    ):
        for i, n in enumerate(["High C, Low I, Low A", "High A, Low I, Low C"]):
            with Path(f"processed_data/{dataset_name}/test_cvss_{i + 1}.json").open(
                "rb",
            ) as f:
                test_cvss = json.load(f)
                model.test(
                    x=test_data,
                    y=test_labels,
                    label_dict=dataset_info["test_labels_dict"],
                    aim_runner=aim_runner,
                    figure_title=f"Test CVSS env {n}",
                    dataset_info=dataset_info,
                    image_format="tikz",
                )


if __name__ == "__main__":
    params = dvc.api.params_show(stages=["test_model", "make_aim"])

    test_model(params)
