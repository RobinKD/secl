"""Make aim stage for dvc experiments."""

import logging
import os
from pathlib import Path
from time import sleep

import dvc
import dvc.api
import psutil
from aim import Run

logging_format = "%(asctime)s - %(levelname)s - Make AIM Runner: %(message)s"
logger = logging.getLogger("make_aim")
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(logging_format))
logger.addHandler(handler)
logger.setLevel(10)


def make_aim(params: dict) -> None:
    """Create aim runner and save parameters."""
    dataset_name = params["dataset"]["name"]

    model_info = params["model"]["name"]

    aim_runner = Run(
        experiment=f"{dataset_name} - {model_info}"
        if params.get("experiment_name") is None
        else f"{params['experiment_name']}",
        system_tracking_interval=None,
    )
    sleep(1)
    with Path("current_aim_run").open("w") as f:
        f.write(aim_runner.hash)

    logger.info("%s", f"Current run hash: {aim_runner.hash}")

    aim_runner["dataset"] = params["dataset"]
    aim_runner["model"] = params["model"]
    aim_runner["dvc_exp"] = os.environ.get("DVC_EXP_NAME", "unknown")

    dvc_process = psutil.Process(pid=os.getppid()).cmdline()
    aim_runner["dvc_process"] = " ".join(dvc_process[1:])

    if not Path("before_train").exists():
        with Path("before_train").open("w") as f:
            f.write("empty")


if __name__ == "__main__":
    params = dvc.api.params_show(stages="make_aim")

    make_aim(params)
