"""DVC use this file to create the dataset."""

import dvc.api

from src.my_exps.datasets import (
    BasicDataset,
    CICIDS2017Dataset,
    NSLKDDDataset,
    UnknownDatasetError,
    UNSWNB15Dataset,
    WADIDataset,
)


def init_dataset_class(
    name: str,
    path: str,
    random_seed: int = 43,
) -> BasicDataset:
    """Initialize dataset."""
    match name:
        case "nslkdd":
            return NSLKDDDataset(path, random_seed)
        case "cicids2017":
            return CICIDS2017Dataset(path, random_seed)
        case "unswnb15":
            return UNSWNB15Dataset(path, random_seed)
        case "wadi":
            return WADIDataset(path, random_seed)
        case _:
            raise UnknownDatasetError


def make_dataset(params: dict) -> None:
    """Create dataset and perform pre-processing."""
    name = params["dataset"]["name"]
    dataset_params = params["dataset"]["parameters"]
    dataset = init_dataset_class(
        name,
        params["dataset"]["path"],
        params["dataset"]["random_seed"],
    )
    if (
        type(dataset) == UNSWNB15Dataset
        and "Additional CVSS params" in params["dataset"]
    ):
        dataset.set_cvss_params(params["dataset"]["Additional CVSS params"])
    dataset.make_dataset(**dataset_params)
    dataset.write_to_file(
        dataset_name=name,
        save_normalizer=params["dataset"].get("save_normalizer", False),
        save_csv=params["dataset"].get("save_csv", False),
    )


if __name__ == "__main__":
    params = dvc.api.params_show(stages="make_dataset")

    make_dataset(params)
