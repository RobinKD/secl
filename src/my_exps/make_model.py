"""Provide function used by dvc to run a stage."""

import json
import pickle
from pathlib import Path

import dvc.api

from src.my_exps.models import (
    AnomalyLearner,
    DeepLearner,
    Learner,
    Odin,
    SupConLearner,
    SupLearner,
    UnknownModelError,
    UnsupLearner,
    OpenWorldBaseline,
    SSLBaseline,
)


def init_model(
    model_type: str,
    model_name: str,
    random_state: int,
    *,
    nb_features: int = 0,
    nb_classes: int = 0,
    class_support: dict[int, int] | None = None,
    learner_params: dict | None = None,
    model_params: dict | None = None,
    lightning_params: dict | None = None,
    with_cve: bool = False,
) -> Learner:
    """Initialize model."""
    match model_type:
        case "SupLearner":
            return SupLearner(
                model_name,
                random_state=random_state,
                model_params=model_params,
                with_cve=with_cve,
            )
        case "UnsupLearner":
            return UnsupLearner(
                model_name,
                random_state=random_state,
                model_params=model_params,
                with_cve=with_cve,
            )
        case "DeepLearner":
            return DeepLearner(
                random_state=random_state,
                nb_classes=nb_classes,
                nb_features=nb_features,
                class_support=class_support,
                with_cve=with_cve,
                learner_parameters=learner_params,
                model_parameters=model_params,
                lightning_parameters=lightning_params,
            )
        case "Odin":
            return Odin(
                random_state=random_state,
                nb_classes=nb_classes,
                nb_features=nb_features,
                class_support=class_support,
                learner_parameters=learner_params,
                model_parameters=model_params,
                lightning_parameters=lightning_params,
            )
        case "SupConLearner":
            return SupConLearner(
                random_state=random_state,
                nb_classes=nb_classes,
                nb_features=nb_features,
                learner_parameters=learner_params,
                model_parameters=model_params,
                lightning_parameters=lightning_params,
            )
        case "AnomalyLearner":
            return AnomalyLearner(
                model_name,
                random_state=random_state,
                model_params=model_params,
            )
        case "OpenWorldBaseline":
            return OpenWorldBaseline(random_state=random_state, **model_params)
        case "SSLBaseline":
            return SSLBaseline(
                random_state=random_state,
                nb_classes=nb_classes,
                nb_features=nb_features,
                class_support=class_support,
                learner_parameters=learner_params,
                model_parameters=model_params,
                lightning_parameters=lightning_params,
            )
        case _:
            raise UnknownModelError


def make_model(params: dict) -> None:
    """Create the model that will be trained."""
    dataset_name = params["dataset"]["name"]

    with Path(f"processed_data/{dataset_name}/dataset_info.json").open("r") as f:
        dataset_info = json.load(f)

    model_type = params["model"]["type"]
    additional_params = {
        "model_params": params["model"].get("model_parameters"),
    }
    if model_type in ["DeepLearner", "Odin", "SupConLearner", "BaselineSSL"]:
        additional_params = {
            **additional_params,
            "nb_features": dataset_info["nb_features"],
            "nb_classes": dataset_info["nb_classes_train"],
            "class_support": dataset_info["train_support"],
            "learner_params": params["model"].get("learner_parameters"),
            "lightning_params": params["model"].get("lightning_parameters"),
        }

    if dataset_info.get("with_cve", False):
        additional_params = {
            **additional_params,
            "with_cve": dataset_info["with_cve"],
        }

    learner = init_model(
        model_type,
        params["model"]["name"],
        params["model"]["random_seed"],
        **additional_params,
    )

    with Path("untrained_model.pkl").open("wb") as f:
        pickle.dump(learner, f)


if __name__ == "__main__":
    params = dvc.api.params_show(stages="make_model")

    make_model(params)
