import json
import logging
import shutil
from pathlib import Path
from typing import TypeVar

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytorch_lightning as pl
import tikzplotlib
import torch
from aim import Image, Run
from pytorch_lightning.callbacks import Callback, EarlyStopping, ModelCheckpoint
from sklearn.metrics import confusion_matrix
from torch import nn
from torch.utils.data import DataLoader

from src.my_exps.datasets.basic_dataset import TorchDataset
from src.my_exps.perf_metrics import macro_average, micro_average
from src.my_exps.utils import (
    MyJsonEncoder,
    get_acc,
    get_perclass_log,
    save_cfm,
    save_hist,
)

from .learner import Learner

logger = logging.getLogger("model_logger")


class BaseResultsCallback(Callback):

    """Callback to track results of Pl modules."""

    def __init__(
        self,
        metrics: dict,
        label_dict: dict[str, int],
        aim_runner: Run,
        *,
        model_name: str = "BaseDeepNN_Callback",
        figure_title: str | None = None,
        image_format: str = "png",
        out_path: Path | None = None,
    ) -> None:
        """Initialize callback parameters."""
        self.model_name = model_name
        self.metrics = metrics
        self.label_dict = label_dict
        self.aim_runner = aim_runner
        self.figure_title = figure_title
        self.image_format = image_format
        self.best_epoch = {step: "0" for step in ["train", "validation", "test"]}
        if out_path:
            self.out_path = out_path

    def stack_output(self, outputs: list[dict]) -> dict:
        """Override parent method."""
        losses = [o["loss"].cpu().detach() for o in outputs]
        losses = np.sum(losses) / len(losses)
        labels = torch.cat([o["labels"].cpu() for o in outputs]).numpy()
        preds = torch.cat([o["preds"].cpu() for o in outputs]).numpy()
        return {"loss": losses, "labels": labels, "preds": preds}

    def on_train_epoch_end(
        self,
        trainer: pl.Trainer,
        pl_module: pl.LightningModule,
    ) -> None:
        """Override parent method."""
        train_results = self.stack_output(pl_module.training_step_outputs)
        self.save_results(
            train_results,
            trainer.current_epoch,
            step="train",
        )
        pl_module.training_step_outputs.clear()

    def on_validation_epoch_end(
        self,
        trainer: pl.Trainer,
        pl_module: pl.LightningModule,
    ) -> None:
        """Override parent method."""
        validation_results = self.stack_output(pl_module.validation_step_outputs)
        self.save_results(
            validation_results,
            trainer.current_epoch,
            step="validation",
        )
        pl_module.validation_step_outputs.clear()

    def on_test_epoch_end(
        self,
        trainer: pl.Trainer,
        pl_module: pl.LightningModule,
    ) -> None:
        """Override parent method."""
        test_results = self.stack_output(pl_module.test_step_outputs)
        self.save_results(
            test_results,
            trainer.current_epoch,
            step="test",
        )
        pl_module.test_step_outputs.clear()

    def collect_metric_results(
        self,
        conf_mat: np.ndarray,
        label_dict: dict,
    ) -> dict:
        """Collect results from metrics in self.metrics."""
        metric_results = {}
        for k, f in self.metrics.items():
            res = f(conf_mat)
            if k not in ["Accuracy", "MCC"]:
                metric_results[k] = get_perclass_log(res, label_dict)
            else:
                metric_results[k] = res
            if k not in ["Support", "Accuracy", "MCC", "TP", "TN"]:
                metric_results["macro_" + k] = macro_average(res)
                metric_results["micro_" + k] = micro_average(res, conf_mat)
        return metric_results

    def collect_figures(
        self,
        normalized_cm: np.ndarray,
        unskewed_cm: np.ndarray,
        label_dict: dict,
        metric_results: dict,
        step: str = "test",
    ) -> dict:
        """Collect figures that will be saved by aim or in files."""
        figs = {}
        if step in ["validation", "test"]:
            figs["normalized_cm"] = save_cfm(
                normalized_cm.tolist(),
                label_dict,
                figure_title=self.figure_title,
            )
            figs["unskewed_cm"] = save_cfm(
                unskewed_cm.tolist(),
                label_dict,
                figure_title=self.figure_title,
            )
            for k in self.metrics:
                if k not in ["Support", "Accuracy", "MCC", "TP", "TN"]:
                    figs[f"{k}_hist"] = save_hist(
                        metric_results["macro_" + k],
                        metric_results["micro_" + k],
                        metric_results[k],
                        k,
                        figure_title=self.figure_title,
                    )
        return figs

    def create_results(
        self,
        epoch_results: dict,
    ) -> dict:
        """Create results that will be saved in save_results."""
        results = {}
        loss = epoch_results["loss"]
        labels = epoch_results["labels"]
        preds = epoch_results["preds"]
        label_dict = {k: v for k, v in self.label_dict.items() if v in labels}
        conf_mat = confusion_matrix(labels, preds)
        unskew_conf_mat = np.divide(
            conf_mat,
            conf_mat.sum(axis=0),
            np.zeros_like(conf_mat, dtype=float),
            where=conf_mat.sum(axis=0) != 0,
        )
        norm_conf_mat = (conf_mat.T / conf_mat.sum(axis=1)).T
        results["loss"] = loss
        results["label_dict"] = label_dict
        results["conf_mat"] = conf_mat
        results["unskewed_cm"] = unskew_conf_mat
        results["normalized_cm"] = norm_conf_mat
        results["FA/AR"] = unskew_conf_mat[0, 1:]
        results["metrics"] = self.collect_metric_results(conf_mat, label_dict)
        return results

    def save_aim_results(
        self,
        step: str,
        epoch: int,
        loss: float,
        metric_results: dict,
        figs: dict,
        *,
        additional_aim_ctx: dict | None = None,
    ) -> None:
        """Save results to aim."""
        aim_ctx = {"context": {"subset": step}, "epoch": epoch}
        if additional_aim_ctx:
            aim_ctx["context"].update(additional_aim_ctx)
        self.aim_runner.track(value=loss, name="Loss", **aim_ctx)
        for k in metric_results:
            if "macro" in k or "micro" in k or k in ["Accuracy", "MCC"]:
                self.aim_runner.track(
                    value=metric_results[k],
                    name=f"{k}",
                    **aim_ctx,
                )
            elif k not in ["TP", "TN", "Support"]:
                for c in metric_results[k]:
                    aim_ctx["context"]["class"] = c
                    self.aim_runner.track(
                        metric_results[k][c],
                        name=f"{k}",
                        **aim_ctx,
                    )
                # Remove class context for next metric (potentially macro/micro)
                del aim_ctx["context"]["class"]

        for name, fig in figs.items():
            self.aim_runner.track(
                Image(fig, format="png"),
                name=name,
                **aim_ctx,
            )

    def save_dvc_results(
        self,
        step: str,
        epoch: int,
        results: dict,
    ) -> None:
        """Save results in files tracked by dvc."""
        full_path = (
            f"save_experiments/{'test' if step == 'test' else 'train'}"
            f"/{step}_full_results.json"
        )
        try:
            with Path(full_path).open("r") as f:
                saved_results = json.load(f)
        except FileNotFoundError:
            saved_results = {}

        with Path(full_path).open("w") as f:
            saved_results[str(epoch)] = {
                "Epoch": epoch,
                **results,
            }
            f.write(json.dumps(saved_results, indent=4, cls=MyJsonEncoder))

        if (
            self.best_epoch[step] in saved_results
            and saved_results[str(epoch)]["Loss"]
            <= saved_results[self.best_epoch[step]]["Loss"]
        ):
            self.best_epoch[step] = str(epoch)
            with Path(f"save_experiments/{step}_results.json").open("w") as f:
                # Save only best epoch
                condensed_res = {
                    k: v
                    for k, v in saved_results[self.best_epoch[step]].items()
                    if "macro" in k
                    or "micro" in k
                    or k in ["Accuracy", "MCC", "Epoch", "Loss"]
                }
                f.write(json.dumps(condensed_res, indent=4, cls=MyJsonEncoder))

    def save_figs(self, figs: dict) -> None:
        """Save figures in file in a specified format."""
        try:
            self.out_path.mkdir(exist_ok=True)
            for name, fig in figs.items():
                match self.image_format:
                    case "png":
                        fig.savefig(f"{self.out_path}/{name}.png", format="png")
                    case "svg":
                        fig.savefig(f"{self.out_path}/{name}.svg", format="svg")
                    case "tikz":
                        plt.figure(fig)
                        tikzplotlib.save(f"{self.out_path}/{name}.tex")
                    case _:
                        pass
        except AttributeError:
            logger.warning(
                "No out_path, figures won't be saved locally.",
                extra={"model_name": self.model_name},
            )

    def save_results(
        self,
        epoch_results: dict,
        epoch: int,
        step: str = "test",
    ) -> None:
        """Save results.

        Log to Aim Run, and save in JSON files for DVC.
        """
        results = self.create_results(epoch_results)
        results["metrics"] = self.collect_metric_results(
            results["conf_mat"],
            results["label_dict"],
        )
        results["figs"] = self.collect_figures(
            results["normalized_cm"],
            results["unskewed_cm"],
            results["label_dict"],
            results["metrics"],
            step,
        )
        self.save_aim_results(
            step,
            epoch,
            results["loss"],
            results["metrics"],
            results["figs"],
        )
        self.save_dvc_results(
            step,
            epoch,
            {"Loss": results["loss"], **results["metrics"]},
        )
        if step == "test":
            self.save_figs(results["figs"])


ResultCallbacks = TypeVar("ResultCallbacks", bound=BaseResultsCallback)


def get_loss_weight(
    weight_type: str,
    class_support: dict[int, int] | None,
    model_name: str,
) -> torch.Tensor | None:
    """Get loss weight for training with Cross-Entropy."""
    if class_support is not None:
        class_support = np.fromiter(class_support.values(), dtype=int)
        if weight_type == "class weights":
            return (
                1
                / torch.tensor([c / sum(class_support) for c in class_support]).float()
            )
        if weight_type == "log class weights":
            return (
                1
                / torch.tensor(
                    np.log([c / sum(class_support) for c in class_support]),
                ).float()
            )

        logger.warning("Incorrect loss weight type", extra={"model_name": model_name})
        return None

    logger.warning("Class support is None", extra={"model_name": model_name})
    return None


class DeepNN(nn.Module):

    """DeepNN class that inherit pytorch's nn.Module."""

    def __init__(
        self,
        *layer_sizes: int,
        dropout: bool = False,
        dropout_proba: float = 0.2,
    ) -> None:
        super().__init__()
        layer_list = []
        self.nb_hidden = len(layer_sizes) - 2
        for i in range(self.nb_hidden + 1):
            input_size = layer_sizes[i]
            curr_size = layer_sizes[i + 1]
            layer_list.append(nn.Linear(input_size, curr_size))
            if i < self.nb_hidden:
                if dropout:
                    layer_list.append(nn.Dropout(dropout_proba))
                layer_list.append(nn.ReLU(inplace=False))
        self.net = nn.Sequential(*layer_list)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Override parent method."""
        return self.net(x)

    def predict_proba(self, x: pd.DataFrame | np.ndarray) -> np.ndarray:
        """For compatibility with lime and sklearn models."""
        self.net.to(device=torch.device("cpu"))
        x_forward = torch.tensor([])
        if isinstance(x, pd.DataFrame):
            x_forward = torch.from_numpy(x.to_numpy()).float()
        elif isinstance(x, np.ndarray):
            x_forward = torch.from_numpy(x).float()
        return nn.functional.softmax(self.forward(x_forward), dim=1).detach().numpy()

    def last_hidden_rep(self, x: torch.Tensor) -> dict:
        """Return the representation of the last hidden layer.

        Before ReLU activation.
        """
        return {"last_hidden": self.net[:-2](x).detach().numpy()}

    def hidden_rep(self, x: torch.Tensor, layer: int) -> dict:
        """Return the representation at the hidden layer specified by parameters."""
        if isinstance(layer, int):
            raise TypeError
        if not layer * 2 - 1 >= self.nb_hidden * 2 - 1:
            return {
                "hidden_" + str(layer): self.net[: layer * 2 - 1](x).detach().numpy(),
            }
        if layer * 2 - 1 > self.nb_hidden * 2 - 1:
            logger.warning(
                "Index too high, you will get the last hidden layer",
                extra={"model_name": "DeepNN"},
            )
        return {"last_hidden": self.net[:-2](x).detach().numpy()}

    def all_hidden_rep(self, x: torch.Tensor) -> dict:
        """Return a list of all hidden representations."""
        names = ["hidden_" + str(i) for i in range(1, self.nb_hidden + 1)]
        names[-1] = "last_hidden"
        return dict(
            zip(
                names,
                [
                    self.net[: layer * 2 + 1](x).detach().numpy()
                    for layer in range(self.nb_hidden)
                ],
                strict=True,
            ),
        )


class BasePlDeepNN(pl.LightningModule):

    """Base PlDeepNN that interfaces with PyTorch Lightning's LightningModule."""

    def __init__(
        self,
        neuralnet: DeepNN,
        *,
        nb_epochs: int = 100,
        validation_frequency: int = 5,
        optim: dict | None = None,
    ) -> None:
        super().__init__()
        self.save_hyperparameters()

        self.model = neuralnet
        self.model_name = "PlDeepNN"

        self.training_step_outputs = []
        self.validation_step_outputs = []
        self.test_step_outputs = []

        self.validation_frequency = validation_frequency
        self.max_epochs = nb_epochs

        try:
            self.lr_scheduler_name = optim["lr_scheduler"]["name"]
            self.lr_scheduler_parameters = optim["lr_scheduler"]["params"]
        except (TypeError, KeyError) as e:
            logger.warning(
                "%s",
                f"Missing scheduler parameters: {e}\nUsing ReduceLROnPlateau...",
                extra={"model_name": self.model_name},
            )
            self.lr_scheduler_name = "ReduceLROnPlateau"
            self.lr_scheduler_parameters = {}

        try:
            self.optimizer_name = optim["optimizer"]["name"]
            self.optimizer_parameters = optim["optimizer"]["params"]
        except (TypeError, KeyError) as e:
            logger.warning(
                "%s",
                f"Lacking optimizer parameters: {e}\n Using Adam optimizer...",
                extra={"model_name": self.model_name},
            )
            self.optimizer_name = "Adam"
            self.optimizer_parameters = {}

        self.epoch_results = {"train": {}, "validation": {}, "test": {}}

    def evaluate(self, batch: torch.Tensor, stage: str | None = None) -> dict:
        """Override parent class."""
        x, y = batch
        outs = self.model(x)
        loss = self.loss(outs, y)
        preds = torch.argmax(outs, dim=1)
        acc = get_acc(preds, y)

        if stage:
            self.log(f"{stage}_loss", loss, prog_bar=True)
            self.log(f"{stage}_acc", acc, prog_bar=True)

        return {"loss": loss, "labels": y, "preds": preds}

    def training_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        res = self.evaluate(batch, "train")
        self.training_step_outputs.append(res)
        return res

    def validation_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        res = self.evaluate(batch, "val")
        self.validation_step_outputs.append(res)
        return res

    def test_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        res = self.evaluate(batch, "test")
        self.test_step_outputs.append(res)
        return res

    def set_training_classes(self) -> None:
        raise NotImplementedError

    def configure_optimizers(self) -> dict:
        """Override parent class."""
        if self.optimizer_name == "Adam":
            optimizer = torch.optim.Adam(self.parameters(), **self.optimizer_parameters)
        elif self.optimizer_name == "AdamW":
            optimizer = torch.optim.AdamW(
                self.parameters(),
                **self.optimizer_parameters,
            )
        else:
            logger.warning(
                "Incorrect optimizer name, default to Adam",
                extra={"model_name": self.model_name},
            )

            optimizer = torch.optim.Adam(self.parameters(), **self.optimizer_parameters)

        if self.lr_scheduler_name == "ReduceLROnPlateau":
            lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
                optimizer,
                **self.lr_scheduler_parameters,
            )
        else:
            logger.warning(
                "Incorrect lr scheduler name, default to ReduceLROnPlateau",
                extra={"model_name": self.model_name},
            )
            lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer)
        lr_scheduler_config = {
            # REQUIRED: The scheduler instance
            "scheduler": lr_scheduler,
            # The unit of the scheduler's step size, could also be 'step'.
            # 'epoch' updates the scheduler on epoch end whereas 'step'
            # updates it after a optimizer update.
            "interval": "epoch",
            # How many epochs/steps should pass between calls to
            # `scheduler.step()`. 1 corresponds to updating the learning
            # rate after every epoch/step.
            "frequency": 3,
            # Metric to to monitor for schedulers like `ReduceLROnPlateau`
            "monitor": "train_loss",
            # If set to `True`, will enforce that the value specified 'monitor'
            # is available when the scheduler is updated, thus stopping
            # training if not found. If set to `False`, it will only produce a warning
            "strict": True,
            # If using the `LearningRateMonitor` callback to monitor the
            # learning rate progress, this keyword can be used to specify
            # a custom logged name
            "name": None,
        }
        return {"optimizer": optimizer, "lr_scheduler": lr_scheduler_config}


class BaseDeepLearner(Learner):

    """Base Deep Learner that can be inherited by other deep learners."""

    def __init__(
        self,
        random_state: int = 43,
        *,
        train_batch_size: int = 256,
        test_batch_size: int = 128,
        with_cve: bool = False,
    ) -> None:
        """Initialize path, model, train and result attributes."""
        super().__init__(with_cve=with_cve)
        pl.seed_everything(random_state)
        self.random_seed = random_state
        self.train_batch_size = train_batch_size
        self.test_batch_size = test_batch_size

    def get_model(self) -> DeepNN:
        """Getter for the DeepNN model."""
        return self.model.model

    def set_ckpt_path(self, path: str) -> None:
        """Set checkpoint path."""
        self.ckpt_path = path

    def train(
        self,
        torch_train: TorchDataset,
        torch_valid: TorchDataset,
        results_callback: BaseResultsCallback,
        checkpoint_callback: ModelCheckpoint | None = None,
        earlystopping_callback: EarlyStopping | None = None,
    ) -> None:
        """Train routine of the DeepLearner."""
        loader_train = DataLoader(
            torch_train,
            batch_size=self.train_batch_size,
            shuffle=False,
            num_workers=4,
            pin_memory=True,
            sampler=None,
        )
        loader_valid = DataLoader(
            torch_valid,
            batch_size=self.test_batch_size,
            shuffle=False,
            num_workers=4,
            pin_memory=True,
            sampler=None,
        )
        if checkpoint_callback is None:
            checkpoint_callback = ModelCheckpoint(
                dirpath=self.ckpt_path,
                filename="model_checkpoint",
                save_top_k=1,
                monitor="val_loss",
                mode="min",
            )

        if earlystopping_callback is None:
            earlystopping_callback = EarlyStopping(
                monitor="val_loss",
                mode="min",
                patience=3,
            )

        trainer = pl.Trainer(
            accelerator="auto",
            devices=1,
            logger=False,
            check_val_every_n_epoch=self.model.validation_frequency,
            num_sanity_val_steps=-1,
            callbacks=[
                checkpoint_callback,
                earlystopping_callback,
                results_callback,
            ],
            max_epochs=self.model.max_epochs,
        )
        trainer.fit(
            self.model,
            train_dataloaders=loader_train,
            val_dataloaders=loader_valid,
        )

    def test(
        self,
        torch_test: TorchDataset,
        results_callback: BaseResultsCallback,
    ) -> None:
        """Test routine of the DeepLearner."""
        loader_test = DataLoader(
            torch_test,
            batch_size=self.test_batch_size,
            shuffle=False,
            num_workers=4,
            pin_memory=True,
            sampler=None,
        )
        trainer = pl.Trainer(
            accelerator="auto",
            devices=1,
            logger=False,
            callbacks=[
                results_callback,
            ],
        )
        trainer.test(self.model, loader_test)

    def save_model(self, path: str) -> None:
        """Move model checkpoint to correct path."""
        shutil.move(path + "/model_checkpoint.ckpt", path + "/trained_model.ckpt")

    def load_model(self, path: str) -> None:
        """Load model checkpoint."""
        self.model = BasePlDeepNN.load_from_checkpoint(path + "/trained_model.ckpt")
