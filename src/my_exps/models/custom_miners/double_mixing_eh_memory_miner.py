import torch

from .eh_memory_miner import EasyHardMemoryMiner


class DoubleMixingEasyHardMemoryMiner(EasyHardMemoryMiner):

    """Mix normal with attacks to further separate them."""

    def __init__(
        self,
        mixing_alpha: float,
        mixing_delta: float = 0.05,
        **kwargs: dict,
    ) -> None:
        super().__init__(**kwargs)

        self.mixing_alpha = mixing_alpha
        self.mixing_delta = mixing_delta

    def get_ref_embeds_labels(self) -> dict[torch.Tensor]:
        """Override parent method."""
        ref_embs, ref_labels = super().get_ref_embeds_labels()

        new_embeds = ref_embs.clone()
        distances = self.distance(ref_embs, new_embeds).fill_diagonal_(0)

        mask_same_class = ref_labels.unsqueeze(1) == ref_labels
        distances[mask_same_class] = 0
        if self.distance.is_inverted:
            closest = torch.argmax(distances, dim=1)
        else:
            distances = torch.where(distances > 0, distances, distances.max())
            closest = torch.argmin(distances, dim=1)
        new_embeds_same_class = (self.mixing_alpha + self.mixing_delta) * ref_embs + (
            1 - self.mixing_alpha - self.mixing_delta
        ) * new_embeds[closest]
        new_embeds_other_class = (self.mixing_alpha - self.mixing_delta) * ref_embs + (
            1 - self.mixing_alpha + self.mixing_delta
        ) * new_embeds[closest]
        return (
            torch.cat((new_embeds_same_class, new_embeds_other_class)),
            torch.cat((ref_labels, ref_labels[closest])),
        )
