import torch

from .eh_memory_miner import EasyHardMemoryMiner


class EasyHardMemoryBinMiner(EasyHardMemoryMiner):

    """Addition of a memory to EasyHardMiner."""

    def mine(
        self,
        embeddings: torch.Tensor,
        labels: torch.Tensor,
        ref_emb: torch.Tensor,
        ref_labels: torch.Tensor,
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """Override parent method."""
        labels_bin = torch.where(labels != 0, 1, 0)
        return super().mine(embeddings, labels_bin, ref_emb, ref_labels)
