import torch
from pytorch_metric_learning.miners import BatchEasyHardMiner

from .memory_miner import MemoryMiner


class EasyHardMemoryMiner(BatchEasyHardMiner, MemoryMiner):

    """Addition of a memory to EasyHardMiner."""

    def __init__(
        self,
        instances_per_class: int,
        memory_replacement_proba: float = 0.01,
        **kwargs: dict,
    ) -> None:
        super().__init__(
            **kwargs,
        )
        self.instances_per_class = instances_per_class
        self.memory_replacement_proba = memory_replacement_proba

    def mine(
        self,
        embeddings: torch.Tensor,
        labels: torch.Tensor,
        ref_emb: torch.Tensor,
        ref_labels: torch.Tensor,
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """Override parent method."""
        ap, p, an, n = super().mine(embeddings, labels, ref_emb, ref_labels)
        p += len(labels)
        n += len(labels)
        return (ap, p, an, n)
