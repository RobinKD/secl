import torch


class MemoryMiner:
    def __init__(self):
        """Child class should inherit another miner."""
        raise NotImplementedError

    def save_to_memory(
        self,
        embeddings: torch.Tensor,
        labels: torch.Tensor,
    ) -> None:
        """Save required samples to memory."""
        if not hasattr(self, "memory_labels"):
            self.memory_labels = torch.tensor([], device=labels.device)
        if not hasattr(self, "memory_embs"):
            self.memory_embs = torch.tensor([], device=embeddings.device)
        uniq_labels = torch.unique(labels)
        uniq_labels = uniq_labels[uniq_labels != -1]
        for ul in uniq_labels:
            candidates_labels = torch.cat(
                (self.memory_labels[self.memory_labels == ul], labels[labels == ul]),
            )
            candidates_embeddings = torch.cat(
                (self.memory_embs[self.memory_labels == ul], embeddings[labels == ul]),
            )

            # Shuffle instead of add with probability 0.01
            proba = torch.randint(100, (1,))
            selected_idx = (
                torch.randperm(len(candidates_labels))
                if proba < (self.memory_replacement_proba * 100)
                else torch.tensor(range(len(candidates_labels)))
            )

            self.memory_embs = torch.cat(
                (
                    self.memory_embs[self.memory_labels != ul],
                    candidates_embeddings[selected_idx][: self.instances_per_class],
                ),
            )
            self.memory_labels = torch.cat(
                (
                    self.memory_labels[self.memory_labels != ul],
                    candidates_labels[selected_idx][: self.instances_per_class],
                ),
            )

    def get_ref_embeds_labels(
        self,
        **_: dict,
    ) -> dict[torch.Tensor]:
        """Return memory as reference embeds and labels."""
        return self.memory_embs, self.memory_labels.long()
