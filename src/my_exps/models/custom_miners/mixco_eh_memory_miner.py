import torch

from .eh_memory_miner import EasyHardMemoryMiner


class MixcoEasyHardMemoryMiner(EasyHardMemoryMiner):

    """Implement Mixco mixing and return related anchors."""

    def __init__(
        self,
        mixing_alpha: float,
        **kwargs: dict,
    ) -> None:
        if "pos_strategy" in kwargs:
            del kwargs["pos_strategy"]
        if "neg_strategy" in kwargs:
            del kwargs["neg_strategy"]
        super().__init__(
            pos_strategy="all",
            neg_strategy="all",
            **kwargs,
        )

        self.mixing_alpha = mixing_alpha

    def get_ref_embeds_labels(self) -> dict[torch.Tensor]:
        """Override parent method."""
        ref_embs, ref_labels = super().get_ref_embeds_labels()

        new_embeds = ref_embs.clone()
        distances = self.distance(ref_embs, new_embeds).fill_diagonal_(0)

        mask_same_class = ref_labels.unsqueeze(1) == ref_labels
        distances[mask_same_class] = 0
        if self.distance.is_inverted:
            closest = torch.argmax(distances, dim=1)
        else:
            distances = torch.where(distances > 0, distances, distances.max())
            closest = torch.argmin(distances, dim=1)
        new_embeds = (
            self.mixing_alpha * ref_embs + (1 - self.mixing_alpha) * new_embeds[closest]
        )
        return new_embeds, ref_labels

    def get_mixed_embs_labels(
        self,
        embs: torch.Tensor,
        labels: torch.Tensor,
    ) -> tuple:
        """Return mixed embeddings (with random embed) and indices."""
        rand_perm = torch.randperm(
            len(embs),
            device=embs.device,
        )
        new_embeds = embs.clone()[rand_perm]
        anchors = torch.tensor(
            range(len(embs)),
            device=embs.device,
        )
        mat_i = anchors.unsqueeze(1) == anchors
        mat_k = anchors.unsqueeze(1) == rand_perm
        anchors_neg_i, neg_i = (~mat_i).nonzero(as_tuple=True)
        anchors_neg_k, neg_k = (~mat_k).nonzero(as_tuple=True)

        new_embeds = self.mixing_alpha * embs + (1 - self.mixing_alpha) * new_embeds
        return (
            new_embeds,
            labels,
            (anchors, anchors, anchors_neg_i, neg_i),
            (anchors, rand_perm, anchors_neg_k, neg_k),
        )
