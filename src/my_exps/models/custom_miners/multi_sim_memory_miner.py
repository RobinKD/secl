import torch
from pytorch_metric_learning.distances import BaseDistance
from pytorch_metric_learning.miners import MultiSimilarityMiner

from .memory_miner import MemoryMiner


class MultiSimilarityMemoryMiner(MultiSimilarityMiner, MemoryMiner):

    """Add memory to MultiSimilarityMiner."""

    def __init__(
        self,
        instances_per_class: int,
        epsilon: float,
        distance: BaseDistance | None,
        memory_replacement_proba: float = 0.01,
    ) -> None:
        super().__init__(epsilon, distance=distance)
        self.instances_per_class = instances_per_class
        self.memory_replacement_proba = memory_replacement_proba

    def mine(
        self,
        embeddings: torch.Tensor,
        labels: torch.Tensor,
        ref_emb: torch.Tensor,
        ref_labels: torch.Tensor,
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """Override parent method."""
        return super().mine(embeddings, labels, ref_emb, ref_labels)
