import torch

from .eh_memory_miner import EasyHardMemoryMiner


class NegativeMixingEasyHardMemoryMiner(EasyHardMemoryMiner):

    """Addition of a memory and mixing to EasyHardMiner."""

    def __init__(
        self,
        mixing_alpha: float,
        **kwargs: dict,
    ) -> None:
        super().__init__(**kwargs)

        self.mixing_alpha = mixing_alpha

    def get_ref_embeds_labels(self) -> dict[torch.Tensor]:
        """Override parent method."""
        ref_embs, ref_labels = super().get_ref_embeds_labels()

        new_embeds = ref_embs.clone()
        distances = self.distance(ref_embs, new_embeds).fill_diagonal_(0)

        mask_same_class = ref_labels.unsqueeze(1) == ref_labels
        distances[mask_same_class] = 0
        if self.distance.is_inverted:
            closest = torch.argmax(distances, dim=1)
        else:
            distances = torch.where(distances > 0, distances, distances.max())
            closest = torch.argmin(distances, dim=1)
        new_embeds = (
            self.mixing_alpha * ref_embs + (1 - self.mixing_alpha) * new_embeds[closest]
        )
        return new_embeds, ref_labels[closest]
