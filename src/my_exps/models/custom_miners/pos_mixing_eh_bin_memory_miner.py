import torch

from .pos_mixing_eh_memory_miner import PositiveMixingEasyHardMemoryMiner


class PositiveMixingEasyHardMemoryBinMiner(PositiveMixingEasyHardMemoryMiner):

    """Perform normal mixing but mine with binary labels."""

    def mine(
        self,
        embeddings: torch.Tensor,
        labels: torch.Tensor,
        ref_emb: torch.Tensor,
        ref_labels: torch.Tensor,
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """Override parent method."""
        labels_bin = torch.where(labels != 0, 1, 0)
        return super().mine(embeddings, labels_bin, ref_emb, ref_labels)
