import torch

from .eh_memory_miner import EasyHardMemoryMiner


class PositiveMixingEasyHardMemoryMiner(EasyHardMemoryMiner):

    """Mix normal with attacks to further separate them."""

    def __init__(
        self,
        mixing_alpha: float,
        **kwargs: dict,
    ) -> None:
        super().__init__(**kwargs)

        self.mixing_alpha = mixing_alpha

    def get_ref_embeds_labels(
        self,
        embeds: torch.Tensor,
    ) -> dict[torch.Tensor]:
        """Override parent method."""
        ref_embs, ref_labels = super().get_ref_embeds_labels()

        new_embeds = torch.cat((ref_embs.clone(), embeds))
        distances = self.distance(new_embeds, new_embeds).fill_diagonal_(0)

        mask_same_class = ref_labels.unsqueeze(1) == ref_labels
        distances[mask_same_class.nonzero(as_tuple=True)] = 0
        distances[:, len(ref_labels) :] = 0
        if self.distance.is_inverted:
            closest = torch.argmax(distances, dim=1)
        else:
            distances = torch.where(distances > 0, distances, distances.max())
            closest = torch.argmin(distances, dim=1)
        new_embeds = (
            self.mixing_alpha * ref_embs[closest]
            + (1 - self.mixing_alpha) * new_embeds[closest]
        )
        return new_embeds, ref_labels[closest]
