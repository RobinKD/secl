import torch
from pytorch_metric_learning.miners import BatchEasyHardMiner


class PrototypeMiner(BatchEasyHardMiner):

    """Use class prototypes as positive instances."""

    def __init__(
        self,
        allowed_pos_range: tuple[float, float] | None = None,
        allowed_neg_range: tuple[float, float] | None = None,
        prototype_update_lambda: float = 0.9,
        **kwargs: dict,
    ) -> None:
        if allowed_pos_range is not None:
            allowed_pos_range = tuple(allowed_pos_range)
        if allowed_neg_range is not None:
            allowed_neg_range = tuple(allowed_neg_range)
        super().__init__(
            allowed_pos_range=allowed_pos_range,
            allowed_neg_range=allowed_neg_range,
            **kwargs,
        )
        self.prototype_update_lambda = prototype_update_lambda

    def initialize_prototypes(self, classes: list[int], size_inputs: int) -> None:
        """Initialize class prototypes."""
        self.prototype_classes = dict(enumerate(classes))
        self.prototypes = torch.randn((len(classes), size_inputs))

    def update_prototypes(self, embeddings: torch.Tensor, labels: torch.Tensor) -> None:
        """Update class prototypes in a moving average style."""
        if self.prototypes.device != embeddings.device:
            self.prototypes = self.prototypes.to(embeddings.device)
        for i, c in self.prototype_classes.items():
            if embeddings[labels == c].shape[0] != 0:
                self.prototypes[i] = self.prototype_update_lambda * self.prototypes[
                    i
                ] + (1 - self.prototype_update_lambda) * embeddings[labels == c].mean(
                    dim=0,
                )

    def get_ref_embeds_labels(self) -> dict[torch.Tensor]:
        """Add memory to embeddings and labels."""
        prototype_classes = torch.tensor(
            list(self.prototype_classes.values()),
            device=self.prototype.device,
        )

        # Add prototype items to batch
        return self.prototypes, prototype_classes

    def mine(
        self,
        embeddings: torch.Tensor,
        labels: torch.Tensor,
        ref_emb: torch.Tensor,
        ref_labels: torch.Tensor,
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """Override parent method."""
        ap, p, an, n = super().mine(embeddings, labels, ref_emb, ref_labels)
        p += len(labels)
        n += len(labels)
        return (ap, p, an, n)
