import logging
from functools import partial

import numpy as np
import pandas as pd
import pytorch_lightning as pl
import torch
import torch.nn.functional as F
from aim import Run
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedShuffleSplit
from torch import nn

from src.my_exps.datasets.basic_dataset import TorchDataset, TorchDatasetWithCVSS
from src.my_exps.perf_metrics import macro_average, micro_average
from src.my_exps.utils import (
    get_acc,
    get_perclass_log,
)

from .common_deep import (
    BaseDeepLearner,
    BasePlDeepNN,
    BaseResultsCallback,
    DeepNN,
    get_loss_weight,
)

logger = logging.getLogger("model_logger")

logger_loss = logging.getLogger("custom_loss_logger")


class TrackResultsCallback(BaseResultsCallback):

    """Callback to track results of Pl modules.

    Integrate the use of CVSS scores.
    """

    def __init__(
        self,
        metrics: dict,
        label_dict: dict[str, int],
        aim_runner: Run,
        *,
        model_name: str = "DeepNN_Callback",
        figure_title: str | None = None,
        image_format: str = "png",
        out_path: str | None = None,
        train_cvss_scores: np.ndarray | None = None,
        valid_cvss_scores: np.ndarray | None = None,
        test_cvss_scores: np.ndarray | None = None,
    ):
        super().__init__(
            metrics,
            label_dict,
            aim_runner,
            model_name=model_name,
            figure_title=figure_title,
            image_format=image_format,
            out_path=out_path,
        )
        self.train_cvss_score = train_cvss_scores
        self.val_cvss_score = valid_cvss_scores
        self.test_cvss_score = test_cvss_scores

    def on_train_epoch_end(
        self,
        trainer: pl.Trainer,
        pl_module: pl.LightningModule,
    ) -> None:
        """Override parent method."""
        train_results = self.stack_output(pl_module.training_step_outputs)
        self.save_results(
            train_results,
            trainer.current_epoch,
            step="train",
            cvss_scores=self.train_cvss_score,
        )
        pl_module.training_step_outputs.clear()

    def on_validation_epoch_end(
        self,
        trainer: pl.Trainer,
        pl_module: pl.LightningModule,
    ) -> None:
        """Override parent method."""
        validation_results = self.stack_output(pl_module.validation_step_outputs)
        self.save_results(
            validation_results,
            trainer.current_epoch,
            step="validation",
            cvss_scores=self.val_cvss_score,
        )
        pl_module.validation_step_outputs.clear()

    def on_test_epoch_end(
        self,
        trainer: pl.Trainer,
        pl_module: pl.LightningModule,
    ) -> None:
        """Override parent method."""
        test_results = self.stack_output(pl_module.test_step_outputs)
        self.save_results(
            test_results,
            trainer.current_epoch,
            step="test",
            cvss_scores=self.test_cvss_score,
        )
        pl_module.test_step_outputs.clear()

    def collect_metric_results(
        self,
        conf_mat: np.ndarray,
        label_dict: dict,
        labels: np.ndarray,
        preds: np.ndarray,
        cvss_scores: np.ndarray | None,
    ) -> dict:
        """Add results with CVSS scores if needed."""
        base_metric_results = super().collect_metric_results(conf_mat, label_dict)
        cvss_results = {}
        if cvss_scores:
            for k in ["FalseAlarmCost", "MissCost", "AttackCost"]:
                if k in self.metrics.items():
                    res = self.metrics[k](labels, preds, label_dict, cvss_scores)
                    cvss_results[k] = get_perclass_log(
                        res,
                        {key: v for key, v in label_dict.items() if key != "normal"},
                    )
                    cvss_results["macro_" + k] = macro_average(res)
                    cvss_results["micro_" + k] = micro_average(res, conf_mat[1:])
        return {**base_metric_results, **cvss_results}

    def create_results(
        self,
        epoch_results: dict,
        cvss_scores: np.ndarray | None,
    ) -> dict:
        """Create results that will be saved in save_results."""
        results = {}
        loss = epoch_results["loss"]
        labels = epoch_results["labels"]
        preds = epoch_results["preds"]
        label_dict = {k: v for k, v in self.label_dict.items() if v in labels}
        conf_mat = confusion_matrix(labels, preds)
        unskew_conf_mat = np.divide(
            conf_mat,
            conf_mat.sum(axis=0),
            np.zeros_like(conf_mat, dtype=float),
            where=conf_mat.sum(axis=0) != 0,
        )
        norm_conf_mat = (conf_mat.T / conf_mat.sum(axis=1)).T
        results["loss"] = loss
        results["label_dict"] = label_dict
        results["conf_mat"] = conf_mat
        results["unskewed_cm"] = unskew_conf_mat
        results["normalized_cm"] = norm_conf_mat
        results["FA/AR"] = unskew_conf_mat[0, 1:]
        results["metrics"] = self.collect_metric_results(
            conf_mat,
            label_dict,
            epoch_results["labels"],
            epoch_results["preds"],
            cvss_scores,
        )
        return results

    def save_results(
        self,
        epoch_results: dict,
        epoch: int,
        step: str = "test",
        cvss_scores: np.ndarray | None = None,
    ) -> None:
        """Override parent method."""
        results = self.create_results(epoch_results, cvss_scores)
        results["metrics"] = self.collect_metric_results(
            results["conf_mat"],
            results["label_dict"],
            epoch_results["labels"],
            epoch_results["preds"],
            cvss_scores,
        )
        results["figs"] = self.collect_figures(
            results["normalized_cm"],
            results["unskewed_cm"],
            results["label_dict"],
            results["metrics"],
            step,
        )
        self.save_aim_results(
            step,
            epoch,
            results["loss"],
            results["metrics"],
            results["figs"],
        )
        self.save_dvc_results(
            step,
            epoch,
            {"Loss": results["loss"], **results["metrics"]},
        )
        self.save_figs(results["figs"])


def basic_cvss_cross_entropy(
    inp: torch.Tensor,
    target: torch.Tensor,
    cvss_scores: torch.Tensor,
) -> torch.Tensor:
    """Compute basic Cross-Entropy using CVSS scores."""
    return cvss_scores * F.cross_entropy(inp, target, reduction="none")


def alpha_cvss_cross_entropy(
    inp: torch.Tensor,
    target: torch.Tensor,
    cvss_scores: torch.Tensor,
    alpha: int,
) -> torch.Tensor:
    """Compute Cross-Entropy using CVSS scores powered by alpha."""
    return cvss_scores.pow(alpha) * F.cross_entropy(inp, target, reduction="none")


def exp_cvss_cross_entropy(
    inp: torch.Tensor,
    target: torch.Tensor,
    cvss_scores: torch.Tensor,
) -> torch.Tensor:
    """Compute basic Cross-Entropy using CVSS scores exponentiated."""
    return cvss_scores.exp() * F.cross_entropy(inp, target, reduction="none")


class CVSSCELoss(nn.Module):

    """Custom Loss implementing the use of CVSS scores."""

    def __init__(self, chosen_loss: str = "Basic", power: int | None = None) -> None:
        super().__init__()
        if chosen_loss == "Alpha" and power is not None:
            logger_loss.info("%s", f"Chosen CVSS Alpha {power} Loss")
            self.CVSS_CE = partial(alpha_cvss_cross_entropy, alpha=power)
        elif chosen_loss == "Exp":
            logger_loss.info("Chosen CVSS Exp Loss")
            self.CVSS_CE = exp_cvss_cross_entropy
        else:
            logger_loss.info("Chosen or defaulted to Basic CVSS Loss")
            self.CVSS_CE = basic_cvss_cross_entropy

    def forward(
        self,
        inp: torch.Tensor,
        target: torch.Tensor,
        cvss_scores: torch.Tensor,
        device: str,
    ) -> torch.Tensor:
        """Compute the loss.

        If target == 0, and inp != 0 -> False alarm, so use class cvss means of inp
        or global mean if class mean doesn't exist
        if target != and inp != target -> Use CVSS if exists, or class mean if not
        """
        # Instances that have a CVSS score (attacks) and were missclassified
        miss_loss = (
            self.CVSS_CE(inp, target, cvss_scores)
            * cvss_scores.ne(-1)
            * inp.argmax(dim=1).ne(target)
        ).sum()
        # Instances (normal or attacks) that were missclassified as attacks:
        class_means_tensor = torch.tensor(
            [
                self.class_mean_CVSS[int(t)]
                if int(t) != self.label_dict["normal"]
                else -1
                for t in target
            ],
            device=device,
        )
        fa_loss = (
            self.CVSS_CE(inp, target, class_means_tensor)
            * target.ne(inp.argmax(dim=1))
            * target.ne(self.label_dict["normal"])
        ).sum()
        # Do not have a CVSS score (normal or attacks) and were missclassified
        normal_ce = (
            F.cross_entropy(inp, target)
            * inp.argmax(dim=1).ne(target)
            * cvss_scores.eq(-1)
        ).sum()
        return miss_loss + fa_loss + normal_ce

    def set_labeldict(self, label_dict: dict) -> None:
        self.label_dict = label_dict

    def set_CVSS_means(self, y: np.ndarray, cvss_scores: np.ndarray) -> None:
        """Compute and set class means of CVSS scores."""
        pd_cvss_scores = pd.Series(cvss_scores)
        self.class_mean_CVSS = {
            c: pd.DataFrame(
                pd_cvss_scores[y == c].replace(-1, np.nan).dropna(axis=0).to_list(),
            )
            .mean()
            .mean()
            for c in self.label_dict.values()
            if c != self.label_dict["normal"]
        }
        global_mean_cvss = (
            pd.DataFrame(pd_cvss_scores.replace(-1, np.nan).dropna(axis=0).to_list())
            .mean()
            .mean()
        )
        for c in self.class_mean_CVSS:
            if c != self.label_dict and np.isnan(self.class_mean_CVSS[c]):
                self.class_mean_CVSS[c] = global_mean_cvss


class PlDeepNN(BasePlDeepNN):

    """Inherit parent class, with additional loss parameters."""

    def __init__(
        self,
        neuralnet: DeepNN,
        *,
        nb_epochs: int = 100,
        validation_frequency: int = 5,
        loss: dict | None = None,
        optim: dict | None = None,
        class_support: dict[int, int] | None = None,
    ):
        super().__init__(
            neuralnet,
            nb_epochs=nb_epochs,
            validation_frequency=validation_frequency,
            optim=optim,
        )
        try:
            loss_name = loss["name"]
            if loss_name == "CrossEntropyLoss":
                self.loss = nn.CrossEntropyLoss(
                    get_loss_weight(
                        loss["weight"],
                        class_support,
                        self.model_name,
                    ),
                )
            elif loss_name == "CVSSCELoss":
                chosen_loss = loss.get("chosen_loss", "Basic")
                power = None
                if chosen_loss == "Alpha":
                    power = loss.get("power", 2)
                self.loss = CVSSCELoss(chosen_loss, power)
        except (TypeError, KeyError) as e:
            logger.warning(
                "%s",
                f"Missing loss parameter {e}\nDefault to basic CrossEntropy",
                extra={"model_name": self.model_name},
            )
            self.loss = nn.CrossEntropyLoss()

    def evaluate(self, batch: torch.Tensor, stage: str | None = None) -> dict:
        """Override parent class."""
        if isinstance(self.loss, CVSSCELoss):
            x, y, cvss_scores = batch
            outs = self.model(x)
            loss = self.loss(outs, y, cvss_scores, self.device)
            preds = torch.argmax(outs, dim=1)
            acc = get_acc(preds, y)

            if stage:
                self.log(f"{stage}_loss", loss, prog_bar=True)
                self.log(f"{stage}_acc", acc, prog_bar=True)

            return {"loss": loss, "labels": y, "preds": preds}

        return super().evaluate(batch, stage)


class DeepLearner(BaseDeepLearner):

    """DeepLearner, inherits BaseDeepLearner."""

    def __init__(
        self,
        nb_features: int,
        nb_classes: int,
        random_state: int = 43,
        *,
        learner_parameters: dict | None = None,
        model_parameters: dict | None = None,
        lightning_parameters: dict | None = None,
        with_cve: bool = False,
        class_support: dict[int, int] | None = None,
    ):
        """Initialize model."""
        if not learner_parameters:
            learner_parameters = {}
        super().__init__(
            random_state,
            with_cve=with_cve,
            **learner_parameters,
        )
        try:
            neuralnet = DeepNN(
                nb_features,
                *model_parameters["hidden_layers"],
                nb_classes,
            )
        except (TypeError, KeyError):
            logger.warning(
                "Missing hidden_layer parameter\n Default to single layer of 256",
                extra={"model_name": self.model_name},
            )
            neuralnet = DeepNN(nb_features, 256, nb_classes)

        self.model = PlDeepNN(
            neuralnet,
            **lightning_parameters,
            class_support=class_support,
        )

    def train(
        self,
        x: np.ndarray,
        y: np.ndarray,
        label_dict: dict,
        aim_runner: Run,
        train_cvss: np.ndarray | None = None,
    ):
        """Override and call parent method."""
        sss = StratifiedShuffleSplit(
            n_splits=1,
            test_size=0.1,
            random_state=self.random_seed,
        )
        train_index, valid_index = next(sss.split(x, y))
        x_train, x_valid, y_train, y_valid = (
            x[train_index],
            x[valid_index],
            y[train_index],
            y[valid_index],
        )

        cvss_train = None
        cvss_valid = None
        if train_cvss is not None:
            cvss_train, cvss_valid = (
                train_cvss[train_index],
                train_cvss[valid_index],
            )

        # Add one sample in valid for classes that are not present
        if len(np.unique(y_train)) != len(np.unique(y_valid)):
            not_present = np.where(~np.isin(np.unique(y_train), np.unique(y_valid)))[0]
            for c in not_present:
                one_sample_index = np.where(y_train == c)[0][0]
                x_valid = np.concatenate(
                    (x_valid, x_train[one_sample_index].reshape(1, -1)),
                    axis=0,
                )
                y_valid = np.concatenate(
                    (y_valid, y_train[one_sample_index].reshape(1)),
                    axis=0,
                )
                if train_cvss is not None:
                    cvss_valid = np.concatenate(
                        (cvss_valid, cvss_train[one_sample_index].reshape(1, -1)),
                        axis=0,
                    )

        torch_train = TorchDataset(x_train, y_train)
        torch_valid = TorchDataset(x_valid, y_valid)
        if train_cvss is not None and isinstance(self.model.loss, CVSSCELoss):
            torch_train = TorchDatasetWithCVSS(x_train, y_train, cvss_train)
            torch_valid = TorchDatasetWithCVSS(x_valid, y_valid, cvss_valid)
            self.model.loss.set_labeldict(label_dict)
            self.model.loss.set_CVSS_means(y_train, cvss_train)

        tracking_metrics_callback = TrackResultsCallback(
            self.metrics,
            label_dict,
            aim_runner,
            train_cvss_scores=cvss_train,
            valid_cvss_scores=cvss_valid,
        )

        super().train(
            torch_train,
            torch_valid,
            tracking_metrics_callback,
        )

    def test(
        self,
        x: np.ndarray,
        y: np.ndarray,
        label_dict: dict,
        aim_runner: Run,
        test_cvss: np.ndarray | None = None,
        image_format: str = "png",
        out_path: str | None = None,
    ):
        """Override and call parent method."""
        torch_test = TorchDataset(x, y)
        if test_cvss is not None and isinstance(self.model.loss, CVSSCELoss):
            torch_test = TorchDatasetWithCVSS(x, y, test_cvss)
            self.model.loss.set_labeldict(label_dict)
            self.model.loss.set_CVSS_means(y, test_cvss)

        tracking_metrics_callback = TrackResultsCallback(
            self.metrics,
            label_dict,
            aim_runner,
            test_cvss_scores=test_cvss,
            image_format=image_format,
            out_path=out_path,
        )
        super().test(
            torch_test,
            tracking_metrics_callback,
        )

    def load_model(self, path: str) -> None:
        """Load model checkpoint."""
        self.model = PlDeepNN.load_from_checkpoint(path + "/trained_model.ckpt")
