import json
import logging
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib
from aim import Image, Text
from sklearn.metrics import confusion_matrix

import src.my_exps.perf_metrics as pm
from src.my_exps.utils import MyJsonEncoder, get_perclass_log, save_cfm, save_hist

logging_format = "%(asctime)s - %(levelname)s - %(model_name)s model: %(message)s"
logger = logging.getLogger("model_logger")
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(logging_format))
logger.addHandler(handler)
logger.setLevel(10)


class UnknownModelError(Exception):

    """Raise Exception when model type is unknown."""

    def __init__(self) -> None:
        """Initialize model error."""
        super().__init__("Unknown Model")


class Learner:

    """Basic Learner class to inherit from."""

    def __init__(self, with_cve=False):
        """Create a Learner instance."""
        self.model = None
        self.model_name = None
        self.metrics = {
            "Support": pm.support,
            "TP": pm.true_positive,
            "TN": pm.true_negative,
            "Accuracy": pm.accuracy,
            "TPR": pm.tpr,
            "TNR": pm.tnr,
            "PPV": pm.ppv,
            "NPV": pm.npv,
            "F1": pm.f1,
            "Informedness": pm.informedness,
            "Markedness": pm.markedness,
            "MCC": pm.mcc,
        }
        if with_cve:
            self.metrics.update(
                {
                    "FalseAlarmCost": pm.false_alarm_costs,
                    "MissCost": pm.miss_attack_costs,
                    "AttackCost": pm.attack_costs,
                },
            )

    def get_model(self):
        return self.model

    def train(self):
        pass

    def test(self):
        pass

    def save_model(self, path):
        with open("{}/trained_model.pkl".format(path), "wb") as f:
            pickle.dump(self.model, f)

    def load_model(self, path):
        with open("{}/trained_model.pkl".format(path), "rb") as f:
            self.model = pickle.load(f)

    def save_results(
        self,
        y,
        y_pred,
        label_dict,
        aim_runner,
        step="test",
        cvss_scores=None,
        figure_title=None,
        image_format="png",
        **kwargs,
    ):
        results = {}
        conf_mat = confusion_matrix(y, y_pred)
        unskew_conf_mat = np.divide(
            conf_mat,
            conf_mat.sum(axis=0),
            np.zeros_like(conf_mat, dtype=float),
            where=conf_mat.sum(axis=0) != 0,
        )
        norm_conf_mat = (conf_mat.T / conf_mat.sum(axis=1)).T
        results["labels_dict"] = label_dict
        results["conf_mat"] = conf_mat.tolist()
        results["unskewed_cm"] = unskew_conf_mat.tolist()
        results["normalized_cm"] = norm_conf_mat.tolist()
        results["FA/AR"] = unskew_conf_mat[0, 1:].tolist()
        for k, f in self.metrics.items():
            if k in ["FalseAlarmCost", "MissCost", "AttackCost"]:
                res = f(y, y_pred, label_dict, cvss_scores)
            else:
                res = f(conf_mat)
            if k in ["FalseAlarmCost", "MissCost", "AttackCost"]:
                results[k] = get_perclass_log(
                    res, {key: v for key, v in label_dict.items() if key != "normal"}
                )
            elif k not in ["Accuracy", "MCC"]:
                results[k] = get_perclass_log(res, label_dict)
            else:
                results[k] = res
            if k not in ["Support", "Accuracy", "MCC", "TP", "TN"]:
                results["macro_" + k] = pm.macro_average(res)
                if k in ["FalseAlarmCost", "MissCost", "AttackCost"]:
                    results["micro_" + k] = pm.micro_average(res, conf_mat[1:])
                else:
                    results["micro_" + k] = pm.micro_average(res, conf_mat)
            for k in results:
                if "macro" in k or "micro" in k or k in ["Accuracy", "MCC"]:
                    aim_runner.track(
                        value=results[k],
                        name="{}".format(k),
                        context={"subset": step},
                    )
                elif k in [
                    "FalseAlarmCost",
                    "MissCost",
                    "AttackCost",
                    "TPR",
                    "TNR",
                    "PPV",
                    "NPV",
                    "F1",
                    "Informedness",
                    "Markedness",
                ]:
                    for c in results[k]:
                        aim_runner.track(
                            results[k][c],
                            context={"subset": step, "class": c},
                            name="{}".format(k),
                        )
        with open("save_experiments/{0}/{0}_full_results.json".format(step), "w") as f:
            f.write(json.dumps(results, indent=4, cls=MyJsonEncoder))

        with open("save_experiments/{}_results.json".format(step), "w") as f:
            condensed_res = {
                k: v
                for k, v in results.items()
                if "macro" in k or "micro" in k or k in ["Accuracy", "MCC"]
            }
            f.write(json.dumps(condensed_res, indent=4, cls=MyJsonEncoder))

        if step == "test":
            figs = {}
            aim_runner.track(
                Text(f"{results['conf_mat']}"),
                name="Full cfm",
            )
            figs["normalized_cm"] = save_cfm(
                results["normalized_cm"], label_dict, figure_title=figure_title
            )
            figs["unskewed_cm"] = save_cfm(
                results["unskewed_cm"], label_dict, figure_title=figure_title
            )
            for k in self.metrics:
                if k not in ["Support", "Accuracy", "MCC", "TP", "TN"]:
                    figs["{}_hist".format(k)] = save_hist(
                        results["macro_" + k],
                        results["micro_" + k],
                        results[k],
                        k,
                        figure_title=figure_title,
                    )
            for name, fig in figs.items():
                if image_format == "svg":
                    aim_runner.track(Image(fig, format="png"), name=name)
                    try:
                        os.makedirs(kwargs["out_path"], exist_ok=True)
                        fig.savefig(
                            "{}/{}.svg".format(kwargs["out_path"], name), format="svg"
                        )
                    except KeyError:
                        logger.exception(
                            "%s",
                            "Save path undefined",
                            extra={"model_name": self.model_name},
                        )
                elif image_format == "tikz":
                    aim_runner.track(Image(fig, format="png"), name=name)
                    try:
                        os.makedirs(kwargs["out_path"], exist_ok=True)
                        plt.figure(fig)
                        tikzplotlib.save(f"{kwargs['out_path']}/{name}.tex")
                    except KeyError:
                        logger.exception(
                            "%s",
                            "Save path undefined",
                            extra={"model_name": self.model_name},
                        )
                else:
                    aim_runner.track(Image(fig, format=image_format), name=name)
