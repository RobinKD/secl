import logging

import numpy as np
import torch
from aim import Run
from sklearn.model_selection import StratifiedShuffleSplit
from torch import nn

from src.my_exps.datasets.basic_dataset import (
    TorchDataset,
    TorchDatasetPercentageLabeled,
)
from src.my_exps.utils import (
    get_acc,
)

from .common_deep import (
    BaseDeepLearner,
    BasePlDeepNN,
    DeepNN,
    get_loss_weight,
)
from .deeplearner import TrackResultsCallback

logger = logging.getLogger("model_logger")

logger_loss = logging.getLogger("custom_loss_logger")


class PlDeepNN(BasePlDeepNN):

    """Inherit parent class, with additional loss parameters."""

    def __init__(
        self,
        neuralnet: DeepNN,
        *,
        nb_epochs: int = 100,
        validation_frequency: int = 5,
        loss: dict | None = None,
        optim: dict | None = None,
        class_support: dict[int, int] | None = None,
    ):
        super().__init__(
            neuralnet,
            nb_epochs=nb_epochs,
            validation_frequency=validation_frequency,
            optim=optim,
        )
        try:
            loss_name = loss["name"]
            if loss_name == "CrossEntropyLoss":
                self.loss = nn.CrossEntropyLoss(
                    get_loss_weight(
                        loss["weight"],
                        class_support,
                        self.model_name,
                    ),
                )
            else:
                raise TypeError
        except (TypeError, KeyError) as e:
            logger.warning(
                "%s",
                f"Missing loss parameter {e}\nDefault to basic CrossEntropy",
                extra={"model_name": self.model_name},
            )
            self.loss = nn.CrossEntropyLoss()

    def training_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        x, y_true, y_train = batch

        x = x[y_train != -1]
        y = y_train[y_train != -1]
        outs = self.model(x)
        loss = self.loss(outs, y)
        preds = torch.argmax(outs, dim=1)
        acc = get_acc(preds, y)

        self.log("train_loss", loss, prog_bar=True)
        self.log("train_acc", acc, prog_bar=True)

        res = {"loss": loss, "labels": y, "preds": preds}
        self.training_step_outputs.append(res)
        return res

    def validation_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        x, y = batch

        outs = self.model(x)
        loss = self.loss(outs, y)
        preds = torch.argmax(outs, dim=1)
        acc = get_acc(preds, y)

        self.log("val_loss", loss, prog_bar=True)
        self.log("val_acc", acc, prog_bar=True)

        res = {"loss": loss, "labels": y, "preds": preds}
        self.validation_step_outputs.append(res)
        return res

    def test_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        x, y = batch

        outs = self.model(x)
        loss = self.loss(outs, y)
        preds = torch.argmax(outs, dim=1)
        acc = get_acc(preds, y)

        self.log("test_loss", loss, prog_bar=True)
        self.log("test_acc", acc, prog_bar=True)

        res = {"loss": loss, "labels": y, "preds": preds}
        self.test_step_outputs.append(res)
        return res


class SSLBaseline(BaseDeepLearner):

    """DeepLearner, inherits BaseDeepLearner."""

    def __init__(
        self,
        nb_features: int,
        nb_classes: int,
        random_state: int = 43,
        *,
        learner_parameters: dict | None = None,
        model_parameters: dict | None = None,
        lightning_parameters: dict | None = None,
        class_support: dict[int, int] | None = None,
    ):
        """Initialize model."""
        if not learner_parameters:
            learner_parameters = {}
        self.percentage_labeled = learner_parameters.pop("percentage_labeled", 1.0)
        super().__init__(
            random_state,
            **learner_parameters,
        )
        try:
            neuralnet = DeepNN(
                nb_features,
                *model_parameters["hidden_layers"],
                nb_classes,
            )
        except (TypeError, KeyError):
            logger.warning(
                "Missing hidden_layer parameter\n Default to single layer of 256",
                extra={"model_name": self.model_name},
            )
            neuralnet = DeepNN(nb_features, 256, nb_classes)

        self.model = PlDeepNN(
            neuralnet,
            **lightning_parameters,
            class_support=class_support,
        )

    def train(
        self,
        x: np.ndarray,
        y: np.ndarray,
        label_dict: dict,
        aim_runner: Run,
    ):
        """Override and call parent method."""
        sss = StratifiedShuffleSplit(
            n_splits=1,
            test_size=0.1,
            random_state=self.random_seed,
        )
        train_index, valid_index = next(sss.split(x, y))
        x_train, x_valid, y_train, y_valid = (
            x[train_index],
            x[valid_index],
            y[train_index],
            y[valid_index],
        )

        # Add one sample in valid for classes that are not present
        if len(np.unique(y_train)) != len(np.unique(y_valid)):
            not_present = np.where(~np.isin(np.unique(y_train), np.unique(y_valid)))[0]
            for c in not_present:
                one_sample_index = np.where(y_train == c)[0][0]
                x_valid = np.concatenate(
                    (x_valid, x_train[one_sample_index].reshape(1, -1)),
                    axis=0,
                )
                y_valid = np.concatenate(
                    (y_valid, y_train[one_sample_index].reshape(1)),
                    axis=0,
                )

        torch_train = TorchDatasetPercentageLabeled(
            x_train,
            y_train,
            self.percentage_labeled,
        )
        torch_valid = TorchDataset(x_valid, y_valid)

        tracking_metrics_callback = TrackResultsCallback(
            self.metrics,
            label_dict,
            aim_runner,
        )

        super().train(
            torch_train,
            torch_valid,
            tracking_metrics_callback,
        )

    def test(
        self,
        x: np.ndarray,
        y: np.ndarray,
        label_dict: dict,
        aim_runner: Run,
        image_format: str = "png",
        out_path: str | None = None,
    ):
        """Override and call parent method."""
        torch_test = TorchDataset(x, y)

        tracking_metrics_callback = TrackResultsCallback(
            self.metrics,
            label_dict,
            aim_runner,
            image_format=image_format,
            out_path=out_path,
        )
        super().test(
            torch_test,
            tracking_metrics_callback,
        )

    def load_model(self, path: str) -> None:
        """Load model checkpoint."""
        self.model = PlDeepNN.load_from_checkpoint(path + "/trained_model.ckpt")
