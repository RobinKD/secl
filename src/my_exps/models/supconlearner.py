"""Define supconlearner and associated classes."""

import json
import logging
from pathlib import Path

import numpy as np
import torch
import torch.nn.functional as F
from aim import Run
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from pytorch_metric_learning import distances
from pytorch_metric_learning.distances.base_distance import BaseDistance
from pytorch_metric_learning.losses import NTXentLoss, SupConLoss
from pytorch_metric_learning.miners import (
    BaseMiner,
)
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.neighbors import KNeighborsClassifier
from torch import nn

import src.my_exps.models.custom_miners as c_miners
from src.my_exps.datasets import TorchDataset, TorchDatasetPercentageLabeled
from src.my_exps.utils import get_acc

from .common_deep import (
    BaseDeepLearner,
    BasePlDeepNN,
    BaseResultsCallback,
    DeepNN,
)

logger = logging.getLogger("model_logger")

logger_supcon = logging.getLogger("supcon_init")


# Torch version
def get_unsup_labels(
    preds: np.ndarray | torch.Tensor,
    labels: np.ndarray | torch.Tensor,
) -> tuple[torch.Tensor, dict]:
    """Get predicted labels of an unsupervised model.

    Chosen prediction is the most present label inside a cluster.
    """
    if isinstance(preds, np.ndarray):
        preds_clone = torch.from_numpy(np.copy(preds))
    else:
        preds_clone = preds.detach().clone()
    if isinstance(labels, np.ndarray):
        labels = torch.from_numpy(labels)
    dict_change = {}
    num_preds_clone = len(torch.unique(labels))
    for label in torch.unique(preds_clone):
        bins = torch.bincount(labels[preds_clone == label], minlength=num_preds_clone)
        dict_change[label] = torch.argmax(bins)
    for key in dict_change:
        #: Use of negative value is to avoid replacing again values
        #: already replaced
        preds_clone[preds_clone == key] = -dict_change[key]
    return -preds_clone, dict_change


def get_distance(
    distance_name: str = "",
    *,
    normalize_embeddings: bool = True,
) -> BaseDistance:
    """Get Pytorch_Metric_Learning distance using its name."""
    try:
        d = getattr(distances, distance_name)
        return d(
            normalize_embeddings=normalize_embeddings,
        )
    except AttributeError:
        logger_supcon.warning("Unknown distance, default to CosineSimilarity")
        return distances.CosineSimilarity()


def get_miner(
    miner_name: str | None = None,
    **miner_parameters: dict | None,
) -> BaseMiner | None:
    try:
        miner = getattr(c_miners, miner_name)
        if "allowed_pos_range" in miner_parameters:
            miner_parameters["allowed_pos_range"] = tuple(
                miner_parameters["allowed_pos_range"],
            )
        if "allowed_neg_range" in miner_parameters:
            miner_parameters["allowed_neg_range"] = tuple(
                miner_parameters["allowed_neg_range"],
            )

        if "distance" in miner_parameters:
            distance = get_distance(**miner_parameters.pop("distance"))
        else:
            distance = get_distance()
        return miner(distance=distance, **miner_parameters)
    except (KeyError, TypeError, AttributeError) as e:
        logger_supcon.warning(
            "%s",
            f"Wrong settings for custom miner: {e}\nNo miner used.",
        )
        return None


class TrackResultsCallback(BaseResultsCallback):
    """Callback to track results of Pl modules.

    Integrate the use of training_classes.
    """

    def __init__(
        self,
        metrics: dict,
        label_dict: dict[str, int],
        aim_runner: Run,
        *,
        model_name: str = "SupCon_Callback",
        figure_title: str | None = None,
        image_format: str = "png",
        out_path: Path | None = None,
        training_classes: dict | None = None,
    ) -> None:
        super().__init__(
            metrics,
            label_dict,
            aim_runner,
            model_name=model_name,
            figure_title=figure_title,
            image_format=image_format,
            out_path=out_path,
        )
        self.training_classes = training_classes

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
    ) -> None:
        """Override parent method."""
        validation_results = self.stack_output(pl_module.validation_step_outputs)
        outs = validation_results["preds"]
        y = validation_results["labels"]
        loss = validation_results["loss"]

        knn = KNeighborsClassifier()
        knn.fit(outs, y)
        kmeans = KMeans(n_clusters=512)
        preds = kmeans.fit_predict(outs)
        cluster_centers = kmeans.cluster_centers_
        label_corresp = dict(enumerate(knn.predict(cluster_centers)))
        preds_to_labels = np.zeros_like(preds)
        for key in label_corresp:
            preds_to_labels[preds == key] = label_corresp[key]
        # preds_to_labels, _ = get_unsup_labels(preds, y)
        acc = get_acc(torch.tensor(preds_to_labels), torch.tensor(y))
        validation_results["preds"] = preds

        pl_module.log("val_loss", loss, prog_bar=True)
        pl_module.log("val_acc", acc, prog_bar=True)
        self.save_results(
            validation_results,
            trainer.current_epoch,
            step="validation",
        )
        pl_module.validation_step_outputs.clear()

    def on_test_epoch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
    ) -> None:
        """Override parent method."""
        test_results = self.stack_output(pl_module.test_step_outputs)
        outs = test_results["preds"]
        y = test_results["labels"]
        loss = test_results["loss"]

        knn = KNeighborsClassifier()
        knn.fit(outs, y)

        # Elbow method
        saved_kmeans = []
        inertias = 0
        for i in [10, 15, 25, 50, 100, 200, 350, 500]:
            kmeans = KMeans(n_clusters=i)
            kmeans.fit(outs)
            # Use inertia to numerically compute elbow best value
            saved_kmeans.append(kmeans)
            inertias.append(kmeans.inertia_)
        best_kmeans = saved_kmeans[np.argmax(np.gradient(np.gradient(inertias)))]

        preds = best_kmeans.fit_predict(outs)
        cluster_centers = best_kmeans.cluster_centers_
        label_corresp = dict(enumerate(knn.predict(cluster_centers)))
        preds_to_labels = np.zeros_like(preds)
        for key in label_corresp:
            preds_to_labels[preds == key] = label_corresp[key]
        # preds_to_labels, _ = get_unsup_labels(preds, y)
        acc = get_acc(torch.tensor(preds_to_labels), torch.tensor(y))
        test_results["preds"] = preds

        pl_module.log("test_loss", loss, prog_bar=True)
        pl_module.log("test_acc", acc, prog_bar=True)
        self.save_results(
            test_results,
            trainer.current_epoch,
            step="test",
        )
        pl_module.test_step_outputs.clear()

    def create_results(
        self,
        epoch_results: dict,
        step: str = "train",
    ) -> dict:
        """Create results that will be saved in save_results."""
        results = {}
        loss = epoch_results["loss"]
        labels = epoch_results["labels"]
        preds = epoch_results["preds"]
        preds, label_corresp = get_unsup_labels(preds, labels)
        if self.training_classes:
            label_dict = {
                **{
                    k: v
                    for k, v in self.label_dict.items()
                    if v in labels and k in self.training_classes
                },
                **{
                    f"{k} (anomaly)": v
                    for k, v in self.label_dict.items()
                    if v in labels and k not in self.training_classes
                },
            }
        else:
            label_dict = {k: v for k, v in self.label_dict.items() if v in labels}
        conf_mat = confusion_matrix(labels, preds)
        unskew_conf_mat = np.divide(
            conf_mat,
            conf_mat.sum(axis=0),
            np.zeros_like(conf_mat, dtype=float),
            where=conf_mat.sum(axis=0) != 0,
        )
        norm_conf_mat = (conf_mat.T / conf_mat.sum(axis=1)).T
        results["loss"] = loss
        results["label_dict"] = label_dict
        results["conf_mat"] = conf_mat
        results["unskewed_cm"] = unskew_conf_mat
        results["normalized_cm"] = norm_conf_mat
        results["FA/AR"] = unskew_conf_mat[0, 1:]
        results["metrics"] = self.collect_metric_results(conf_mat, label_dict)
        label_corresp = {k.item(): v.item() for k, v in label_corresp.items()}
        results["label_corresp"] = {v: [] for v in label_corresp.values()}
        for k, val in label_corresp.items():
            results["label_corresp"][val].append(k)

        return results

    def save_results(
        self,
        epoch_results: dict,
        epoch: int,
        step: str = "test",
    ) -> None:
        """Save results.

        Log to Aim Run, and save in JSON files for DVC.
        """
        results = self.create_results(epoch_results, step)
        results["figs"] = self.collect_figures(
            results["normalized_cm"],
            results["unskewed_cm"],
            results["label_dict"],
            results["metrics"],
            step,
        )
        self.save_aim_results(
            step,
            epoch,
            results["loss"],
            results["metrics"],
            results["figs"],
        )
        self.save_dvc_results(
            step,
            epoch,
            {
                "Loss": results["loss"],
                **results["metrics"],
                "Label_correspondance": results["label_corresp"],
            },
        )
        if step == "test":
            self.save_figs(results["figs"])


def get_vne(embeddings: torch.Tensor):
    # From https://openaccess.thecvf.com/content/CVPR2023/papers/Kim_VNE_An_Effective_Method_for_Improving_Deep_Representation_by_Manipulating_CVPR_2023_paper.pdf
    # Should help regularization
    z = torch.nn.functional.normalize(embeddings, dim=1)
    sing_val = torch.svd(z / np.sqrt(z.shape[0]))[1]
    eig_val = sing_val**2
    return -(eig_val * torch.log(eig_val)).nansum()


class SupConCELoss(nn.Module):
    """Loss mixing SupCon and CE."""

    def __init__(
        self,
        temperature: float,
        distance: BaseDistance,
        alpha: float,
    ) -> None:
        super().__init__()
        self.supcon_loss = SupConLoss(temperature=temperature, distance=distance)
        self.ce_loss = nn.CrossEntropyLoss()
        self.alpha = alpha

    def forward(
        self,
        out: torch.Tensor,
        target: torch.Tensor,
        anchors_masks: tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]
        | None = None,
    ) -> torch.Tensor:
        """Override forward to use the two losses."""
        return self.supcon_loss(out, target, anchors_masks) + self.alpha * self.ce_loss(
            out,
            target.long(),
        )


class SupConBinCELoss(nn.Module):
    """Loss mixing SupCon and CE."""

    def __init__(
        self,
        temperature: float,
        distance: BaseDistance,
        alpha: float,
    ) -> None:
        super().__init__()
        self.supcon_loss = SupConLoss(temperature=temperature, distance=distance)
        self.ce_loss = nn.CrossEntropyLoss()
        self.alpha = alpha

    def forward(
        self,
        out: torch.Tensor,
        target: torch.Tensor,
        anchors_masks: tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]
        | None = None,
    ) -> torch.Tensor:
        """Override forward to use the two losses."""
        target_bin = target.clone()
        target_bin[target_bin != 0] = 1
        return self.supcon_loss(out, target, anchors_masks) + self.alpha * self.ce_loss(
            out,
            target_bin.long(),
        )


class SupConBinSupConLoss(nn.Module):
    """Loss mixing SupCon and CE."""

    def __init__(
        self,
        temperature: float,
        distance: BaseDistance,
        alpha: float,
    ) -> None:
        super().__init__()
        self.supcon_loss = SupConLoss(temperature=temperature, distance=distance)
        self.bin_supcon_loss = SupConLoss(temperature=temperature, distance=distance)
        self.alpha = alpha

    def forward(
        self,
        out: torch.Tensor,
        target: torch.Tensor,
        anchors_masks: tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]
        | None = None,
    ) -> torch.Tensor:
        """Override forward to use the two losses."""
        target_bin = target.clone()
        target_bin[target_bin != 0] = 1
        return self.supcon_loss(
            out, target, anchors_masks
        ) + self.alpha * self.bin_supcon_loss(
            out,
            target_bin,
        )


class BinSupConCELoss(nn.Module):
    """Loss mixing Bin SupCon and Bin CE."""

    def __init__(
        self,
        temperature: float,
        distance: BaseDistance,
        alpha: float,
    ) -> None:
        super().__init__()
        self.bin_supcon_loss = SupConLoss(temperature=temperature, distance=distance)
        self.ce_loss = nn.CrossEntropyLoss()
        self.alpha = alpha

    def forward(
        self,
        out: torch.Tensor,
        target: torch.Tensor,
        anchors_masks: tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]
        | None = None,
    ) -> torch.Tensor:
        """Override forward to use the two losses."""
        target_bin = target.clone()
        target_bin[target_bin != 0] = 1
        return self.bin_supcon_loss(
            out, target_bin, anchors_masks
        ) + self.alpha * self.ce_loss(
            out,
            target.long(),
        )


class BinSupConBinCELoss(nn.Module):
    """Loss mixing Bin SupCon and Bin CE."""

    def __init__(
        self,
        temperature: float,
        distance: BaseDistance,
        alpha: float,
    ) -> None:
        super().__init__()
        self.bin_supcon_loss = SupConLoss(temperature=temperature, distance=distance)
        self.bin_ce_loss = nn.CrossEntropyLoss()
        self.alpha = alpha

    def forward(
        self,
        out: torch.Tensor,
        target: torch.Tensor,
        anchors_masks: tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]
        | None = None,
    ) -> torch.Tensor:
        """Override forward to use the two losses."""
        target_bin = target.clone()
        target_bin[target_bin != 0] = 1
        return self.bin_supcon_loss(
            out, target_bin, anchors_masks
        ) + self.alpha * self.bin_ce_loss(
            out,
            target_bin.long(),
        )


class MixCoLoss(nn.Module):
    """Loss mixing Bin SupCon and Bin CE."""

    def __init__(
        self,
        temperature: float,
        distance: BaseDistance,
        alpha: float = 0.5,
        beta: float = 1.0,
    ) -> None:
        super().__init__()
        self.supcon_loss = SupConLoss(temperature=temperature, distance=distance)
        self.mixco_loss = NTXentLoss(temperature=temperature, distance=distance)
        self.alpha = alpha
        self.beta = beta

    def forward(
        self,
        out: torch.Tensor,
        target: torch.Tensor,
        anchors_masks_supcon: tuple[
            torch.Tensor,
            torch.Tensor,
            torch.Tensor,
            torch.Tensor,
        ]
        | None = None,
        mixed_outs: torch.Tensor | None = None,
        mixed_labels: torch.Tensor | None = None,
        anchors_masks_nce1: tuple[
            torch.Tensor,
            torch.Tensor,
            torch.Tensor,
            torch.Tensor,
        ]
        | None = None,
        anchors_masks_nce2: tuple[
            torch.Tensor,
            torch.Tensor,
            torch.Tensor,
            torch.Tensor,
        ]
        | None = None,
    ) -> torch.Tensor:
        """Override forward to use the two losses."""
        if mixed_outs is not None:
            return self.supcon_loss(
                out,
                target,
                anchors_masks_supcon,
            ) + self.beta * (
                self.alpha
                * self.mixco_loss(
                    mixed_outs,
                    mixed_labels,
                    anchors_masks_nce1,
                )
                + (1 - self.alpha)
                * self.mixco_loss(
                    mixed_outs,
                    mixed_labels,
                    anchors_masks_nce2,
                )
            )
        return self.supcon_loss(
            out,
            target,
            anchors_masks_supcon,
        )


class SupConDeepNN(DeepNN):
    """Override DeepNN to add class projector only during training."""

    def __init__(
        self,
        *layer_sizes: int,
        dropout: bool = False,
        dropout_proba: float = 0.2,
    ) -> None:
        super().__init__(
            *layer_sizes[:-1],
            dropout=dropout,
            dropout_proba=dropout_proba,
        )
        self.projector = nn.Sequential(nn.Linear(layer_sizes[-2], layer_sizes[-1]))

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Override parent method."""
        return F.normalize(self.net(x), 2)

    def project(self, x: torch.Tensor) -> torch.Tensor:
        """Project last hidden layer output to number of classes.

        Used during training.
        """
        return F.normalize(self.projector(x), 2)


class PlDeepNN(BasePlDeepNN):
    """Inherit parent class.

    Add loss, temperature, miner and distance parameters.
    """

    def __init__(
        self,
        neuralnet: DeepNN,
        nb_epochs: int = 100,
        validation_frequency: int = 5,
        optim: dict | None = None,
        distance: dict[str, int | str | float] | None = None,
        miner: dict[str, int | str | float] | None = None,
        loss: dict | None = None,
        temperature: float = 0.1,
        vne_alpha: float | None = None,
    ):
        super().__init__(
            neuralnet,
            nb_epochs=nb_epochs,
            validation_frequency=validation_frequency,
            optim=optim,
        )
        self.model_name = "SupCon_PlDeepNN"

        self.vne_alpha = vne_alpha

        try:
            self.distance = get_distance(**distance)
        except TypeError:
            logger.warning(
                "No distance defined.",
                extra={"model_name": self.model_name},
            )
            self.distance = None
        try:
            loss_dict = {
                "SupConCELoss": SupConCELoss,
                "BinSupConCELoss": BinSupConCELoss,
                "SupConBinCELoss": SupConBinCELoss,
                "SupConBinSupConLoss": SupConBinSupConLoss,
                "BinSupConBinCELoss": BinSupConBinCELoss,
                "MixCoLoss": MixCoLoss,
            }
            loss_name = loss["name"]
            if loss_name == "SupConLoss":
                self.loss = SupConLoss(temperature=temperature, distance=self.distance)
            elif loss_name == "MixCoLoss":
                alpha = loss.get("alpha")
                beta = loss.get("beta")
                self.loss = MixCoLoss(
                    temperature,
                    self.distance,
                    alpha=alpha,
                    beta=beta,
                )
            else:
                alpha = loss["alpha"]
                self.loss = loss_dict[loss_name](temperature, self.distance, alpha)
        except (TypeError, KeyError) as e:
            logger.warning(
                "%s",
                f"Missing loss parameter {e}\nDefault to basic SupConLoss",
                extra={"model_name": self.model_name},
            )
            self.loss = SupConLoss(temperature=temperature, distance=self.distance)
        try:
            self.miner = get_miner(**miner)
        except TypeError:
            logger.warning(
                "No miner defined.",
                extra={"model_name": self.model_name},
            )
            self.miner = None

    def set_training_classes(self, training_classes: dict) -> None:
        """Save training_classes as a class attribute."""
        self.training_classes = training_classes

    def training_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        x, y_true, y_train = batch
        if self.miner:
            try:
                outs = self.model(x)
                if isinstance(self.miner, c_miners.PrototypeMiner):
                    self.miner.update_prototypes(outs.detach(), y_train)

                if isinstance(self.miner, c_miners.MemoryMiner):
                    self.miner.save_to_memory(outs.detach(), y_train)

                ref_embs, ref_labels = self.miner.get_ref_embeds_labels(outs)
            except AttributeError:
                # Not using a custom miner
                outs = self.model(x)
                ref_embs = None
                ref_labels = None

            idx_tuples = self.miner(
                outs,
                y_train,
                ref_embs,
                ref_labels,
            )

            if ref_embs is not None:
                out_loss = torch.cat((outs, ref_embs))
                y_loss = torch.cat((y_train, ref_labels))
            else:
                out_loss = outs.clone()
                y_loss = y_train.clone()
            if not isinstance(self.miner, c_miners.MixcoEasyHardMemoryMiner):
                loss = self.loss(
                    self.model.project(out_loss),
                    y_loss,
                    idx_tuples,
                )
            else:
                (
                    mixed_embs,
                    mixed_labels,
                    idx_tuples_i,
                    idx_tuples_k,
                ) = self.miner.get_mixed_embs_labels(
                    outs[y_train == -1],
                    y_train[y_train == -1],
                )
                loss = self.loss(
                    self.model.project(out_loss),
                    y_loss,
                    idx_tuples,
                    mixed_embs,
                    mixed_labels,
                    idx_tuples_i,
                    idx_tuples_k,
                )
        else:
            outs = self.model(x)
            loss = self.loss(outs, y_true)

        # Add VNE
        if self.vne_alpha is not None:
            loss -= self.vne_alpha * get_vne(self.model.project(outs))

        # Not really true, but much faster than KMeans and still gives
        # a good idea of real performance
        preds = torch.argmax(outs, dim=1)
        preds_to_labels, _ = get_unsup_labels(preds, y_true)
        acc = get_acc(preds_to_labels, y_true)

        self.log("train_loss", loss, prog_bar=True)
        self.log("train_acc", acc, prog_bar=True)

        res = {"loss": loss, "labels": y_true, "preds": preds}
        self.training_step_outputs.append(res)
        return res

    def validation_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        x, y = batch
        outs = self.model(x)
        loss = self.loss(outs, y)

        res = {"loss": loss, "labels": y, "preds": outs}
        self.validation_step_outputs.append(res)
        return res

    def test_step(self, batch: torch.Tensor, batch_idx) -> dict:
        """Override parent class."""
        x, y = batch
        outs = self.model(x)
        loss = self.loss(outs, y)

        res = {"loss": loss, "labels": y, "preds": outs}
        self.test_step_outputs.append(res)
        return res


class SupConLearner(BaseDeepLearner):
    """SupConLearner that inherits BaseDeepLearner.

    To do Supervised Contrastive Learning.
    """

    def __init__(
        self,
        nb_features: int,
        nb_classes: int,
        random_state: int = 43,
        *,
        learner_parameters: dict | None = None,
        model_parameters: dict | None = None,
        lightning_parameters: dict | None = None,
        with_cve: bool = False,
    ) -> None:
        """Initialize model."""
        if not learner_parameters:
            learner_parameters = {}
        self.percentage_labeled = learner_parameters.pop("percentage_labeled", 1.0)
        super().__init__(
            random_state,
            with_cve=with_cve,
            **learner_parameters,
        )
        try:
            hidden_layers = model_parameters.pop("hidden_layers")
            neuralnet = SupConDeepNN(
                nb_features,
                *hidden_layers,
                nb_classes,
                **model_parameters,
            )
        except (TypeError, KeyError):
            logger.warning(
                "Missing model parameter\n"
                "Default to single layer of 256 without"
                " additional parameters.",
                extra={"model_name": self.model_name},
            )
            neuralnet = SupConDeepNN(nb_features, 256, nb_classes)
        self.model = PlDeepNN(
            neuralnet,
            **lightning_parameters,
        )

    def train(
        self,
        x: np.ndarray,
        y: np.ndarray,
        label_dict: dict,
        aim_runner: Run,
    ) -> None:
        """Override and call parent method."""
        self.training_classes = label_dict
        sss = StratifiedShuffleSplit(
            n_splits=1,
            test_size=0.1,
            random_state=self.random_seed,
        )
        train_index, valid_index = next(sss.split(x, y))
        x_train, x_valid, y_train, y_valid = (
            x[train_index],
            x[valid_index],
            y[train_index],
            y[valid_index],
        )

        # Add one sample in valid for classes that are not present
        if len(np.unique(y_train)) != len(np.unique(y_valid)):
            not_present = np.where(~np.isin(np.unique(y_train), np.unique(y_valid)))[0]
            for c in not_present:
                one_sample_index = np.where(y_train == c)[0][0]
                x_valid = np.concatenate(
                    (x_valid, x_train[one_sample_index].reshape(1, -1)),
                    axis=0,
                )
                y_valid = np.concatenate(
                    (y_valid, y_train[one_sample_index].reshape(1)),
                    axis=0,
                )

        torch_train = TorchDatasetPercentageLabeled(
            x_train,
            y_train,
            self.percentage_labeled,
        )
        torch_valid = TorchDataset(x_valid, y_valid)

        tracking_metrics_callback = TrackResultsCallback(
            self.metrics,
            label_dict,
            aim_runner,
        )
        if isinstance(self.model.miner, c_miners.PrototypeMiner):
            self.model.miner.initialize_prototypes(
                label_dict.values(),
                x_train.shape[1],
            )

        checkpoint_callback = ModelCheckpoint(
            dirpath=self.ckpt_path,
            filename="model_checkpoint",
            save_top_k=1,
            monitor="val_acc",
            mode="max",
        )

        earlystopping_callback = EarlyStopping(
            monitor="val_acc",
            mode="max",
            patience=5,
        )
        super().train(
            torch_train,
            torch_valid,
            tracking_metrics_callback,
            checkpoint_callback,
            earlystopping_callback,
        )

    def test(
        self,
        x: np.ndarray,
        y: np.ndarray,
        label_dict: dict,
        aim_runner: Run,
        image_format: str = "png",
        out_path: Path | None = None,
    ) -> None:
        """Override and call parent method."""
        torch_test = TorchDataset(x, y)

        tracking_metrics_callback = TrackResultsCallback(
            self.metrics,
            label_dict,
            aim_runner,
            training_classes=self.training_classes,
            image_format=image_format,
            out_path=out_path,
        )

        self.model.set_training_classes(self.training_classes)

        super().test(
            torch_test,
            tracking_metrics_callback,
        )

    def save_model(self, path: str) -> None:
        """Move model checkpoint to correct path. Also save training_classes."""
        super().save_model(path)
        with Path(f"{path}/training_classes.json").open("w") as f:
            json.dump(self.training_classes, f)

    def load_model(self, path: str) -> None:
        """Load model checkpoint. Also load training_classes."""
        self.model = PlDeepNN.load_from_checkpoint(path + "/trained_model.ckpt")
        with Path(f"{path}/training_classes.json").open("r") as f:
            self.training_classes = json.load(f)
