import os
import time

import graphviz
import numpy as np
from joblib.memory import logging
from sklearn.dummy import DummyClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier, export_graphviz

from .learner import Learner, UnknownModelError

logger = logging.getLogger("model_logger")


def get_model(model):
    """Creates the ML supervised model represented by the parameter model
    Possible models are:
    DummyClassifier, RandomForest, LogisticRegression,
    LinearSVC, GaussianNB, MLP, KNN

    :param model: String
    """

    clf = DummyClassifier(strategy="most_frequent")
    if model == "dummy":
        pass
    elif model == "RandomForest":
        clf = RandomForestClassifier(n_jobs=-1)
    elif model == "LogisticRegression":
        clf = LogisticRegression()
    elif model == "DecisionTree":
        clf = DecisionTreeClassifier()
    elif model == "LinearSVC":
        clf = LinearSVC()
    elif model == "GaussianNB":
        clf = GaussianNB()
    elif model == "MLP":
        clf = MLPClassifier()
    elif model == "KNN":
        clf = KNeighborsClassifier(n_jobs=-1)
    else:
        logger.exception("%s", f"You wrote : {model}", extra={"model_name": model})
        logger.exception(
            "%s",
            "Possible options are:\n"
            "dummy, RandomForest, LogisticRegression, DecisionTree, "
            "LinearSVC, GaussianNB, MLP",
            extra={"model_name": model},
        )
        raise UnknownModelError
    return clf


class SupLearner(Learner):
    """
    Supervised learner class making use of classic shallow ML models
    """

    def __init__(self, model_name, with_cve=False, random_state=43, model_params=None):
        """
        TODO Redo doc
        """
        super().__init__(with_cve)
        self.model = get_model(model_name)
        self.model_name = model_name
        if not self.model.__class__.__name__ == "GaussianNB":
            self.model.set_params(random_state=random_state)
        if model_params:
            if "hidden_layer_sizes" in model_params:
                model_params["hidden_layer_sizes"] = eval(
                    model_params["hidden_layer_sizes"]
                )
            self.model.set_params(**model_params)

    def train(self, x, y, label_dict, aim_runner, train_cvss=None, **train_params):
        begin_time = time.time()
        self.model.fit(x, y)
        logger.info(
            "%s",
            f"Training time: {time.time() - begin_time} sec",
            extra={"model_name": self.model_name},
        )

        y_pred = self.model.predict(x)
        super().save_results(y, y_pred, label_dict, aim_runner, "train", train_cvss)

    def test(self, x, y, label_dict, aim_runner, test_cvss=None, **test_params):
        y_pred = self.model.predict(x)
        self.save_results(
            y, y_pred, label_dict, aim_runner, "test", test_cvss, **test_params
        )

    def save_results(
        self, y, y_pred, label_dict, aim_runner, step="test", cvss_scores=None, **kwargs
    ):
        super().save_results(
            y, y_pred, label_dict, aim_runner, step, cvss_scores, **kwargs
        )
        try:
            os.makedirs(kwargs["out_path"], exist_ok=True)
            if isinstance(self.model, DecisionTreeClassifier):
                try:
                    features = kwargs["dataset_info"]["feature_names"]
                    with open("{}/tree.txt".format(kwargs["out_path"]), "w") as f:
                        n_nodes = self.model.tree_.node_count
                        children_left = self.model.tree_.children_left
                        children_right = self.model.tree_.children_right
                        feature = self.model.tree_.feature
                        threshold = self.model.tree_.threshold
                        max_depth = self.model.tree_.max_depth

                        node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
                        is_leaves = np.zeros(shape=n_nodes, dtype=bool)
                        stack = [
                            (0, 0)
                        ]  # start with the root node id (0) and its depth (0)
                        while len(stack) > 0:
                            # `pop` ensures each node is only visited once
                            node_id, depth = stack.pop()
                            node_depth[node_id] = depth

                            # If the left and right child of a node is not the same we have a split
                            # node
                            is_split_node = (
                                children_left[node_id] != children_right[node_id]
                            )
                            # If a split node, append left and right children and depth to `stack`
                            # so we can loop through them
                            if is_split_node:
                                stack.append((children_left[node_id], depth + 1))
                                stack.append((children_right[node_id], depth + 1))
                            else:
                                is_leaves[node_id] = True

                        f.write(
                            "The binary tree structure has {n} nodes and max depth of {md}".format(
                                n=n_nodes, md=max_depth
                            )
                        )
                        for i in range(n_nodes):
                            if is_leaves[i]:
                                f.write(
                                    "{space}node={node} is a leaf node.\n".format(
                                        space=node_depth[i] * "\t", node=i
                                    )
                                )
                            else:
                                # print("Features:", features)
                                # print("feature:", feature)
                                # print("i:", i)
                                f.write(
                                    "{space}node={node} is a split node: "
                                    "go to node {left} if {feature} <= {threshold} "
                                    "else to node {right}.\n".format(
                                        space=node_depth[i] * "\t",
                                        node=i,
                                        left=children_left[i],
                                        feature=features[feature[i]],
                                        threshold=threshold[i],
                                        right=children_right[i],
                                    )
                                )
                    class_names = list(label_dict)
                    dot_data = export_graphviz(
                        self.model,
                        feature_names=features,
                        class_names=class_names,
                        filled=True,
                    )
                    graph = graphviz.Source(dot_data, format="svg")
                    graph.render(
                        outfile="{}/tree.svg".format(kwargs["out_path"]), format="svg"
                    )
                except AttributeError:
                    logger.exception(
                        "Error for Decision Tree graph: %s",
                        extra={"model_name": self.model_name},
                    )
            if isinstance(self.model, RandomForestClassifier) or isinstance(
                self.model, DecisionTreeClassifier
            ):
                try:
                    feature_imp = [
                        (feature, imp)
                        for feature, imp in zip(
                            kwargs["dataset_info"]["feature_names"],
                            self.model.feature_importances_,
                        )
                    ]
                    feature_imp = sorted(
                        feature_imp, key=lambda tup: tup[1], reverse=True
                    )
                    with open(
                        "{}/feature_importances.txt".format(kwargs["out_path"]), "w"
                    ) as f:
                        f.write("Feature importances:\n{}".format(feature_imp))
                except AttributeError:
                    logger.exception(
                        "%s: Lacking dataset info for feature importance",
                        extra={"model_name": self.model_name},
                    )
        except KeyError:
            logger.exception(
                "%s: Save path undefined",
                extra={"model_name": self.model_name},
            )
