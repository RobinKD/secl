import logging
import time

import numpy as np
from sklearn.cluster import DBSCAN, AgglomerativeClustering, KMeans
from sklearn.dummy import DummyClassifier
from sklearn.mixture import BayesianGaussianMixture

from .learner import Learner, UnknownModelError

logger = logging.getLogger("model_name")


def get_model(model):
    """
    Creates the ML supervised model represented by the parameter model
    Possible models are:
    DummyClassifier, Kmeans, DBSCAN, DPGMM

    :param model: String
    """
    clf = DummyClassifier(strategy="most_frequent")
    if model == "dummy":
        pass
    elif model == "Kmeans":
        clf = KMeans()
    elif model == "dbscan":
        clf = DBSCAN()
    elif model == "AgglomerativeClustering":
        clf = AgglomerativeClustering()
    elif model == "DPGMM":
        clf = BayesianGaussianMixture()
    else:
        logger.exception("%s", f"You wrote : {model}", extra={"model_name": model})
        logger.exception(
            "%s",
            "Possible options are:\n"
            "dummy, Kmeans, DBSCAN, AgglomerativeClustering, DPGMM",
            extra={"model_name": model},
        )
        raise UnknownModelError
    return clf


def get_unsup_labels(labels, y):
    """
    Function to get predicted labels of an unsupervised model
    Chosen prediction is the most present label inside a cluster

    :param labels: array-like, cluster assignement of the model
    :param y: array-like, true labels
    :returns: array_like, the labels predicted

    """
    dict_change = {}
    num_labels = len(np.unique(y))
    for label in np.unique(labels):
        bins = np.bincount(y[labels == label], minlength=num_labels)
        dict_change[label] = np.argmax(bins)
    for lab in dict_change:
        #: Use of negative value is to avoid replacing again values
        #: already replaced
        labels[labels == lab] = -dict_change[lab]
    return -labels


class UnsupLearner(Learner):
    def __init__(self, model_name, with_cve=False, random_state=43, model_params=None):
        """
        TODO Redo doc
        """
        super().__init__(with_cve)
        self.model = get_model(model_name)
        self.model.set_params(random_state=random_state)
        if model_params:
            self.model.set_params(**model_params)

    def train(self, x, y, label_dict, aim_runner, train_cvss=None):
        begin_time = time.time()
        self.model.fit(x, y)
        logger.info(
            "%s",
            f"Training time: {time.time() - begin_time} sec",
            extra={"model_name": self.model_name},
        )

        y_pred = get_unsup_labels(self.model.predict(x), y)
        super().save_results(y, y_pred, label_dict, aim_runner, "train", train_cvss)

    def test(self, x, y, label_dict, aim_runner, test_cvss=None, **kwargs):
        y_pred = get_unsup_labels(self.model.predict(x), y)
        super().save_results(y, y_pred, label_dict, aim_runner, "test", test_cvss)
