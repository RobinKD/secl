

import logging
import numpy as np
import pandas as pd
from torch import isnan

logger = logging.getLogger("dvc")
logger.setLevel(20)


def support(conf_mat):
    return conf_mat.sum(axis=1)


def true_positive(conf_mat):
    return conf_mat.diagonal()


def true_negative(conf_mat):
    return conf_mat.diagonal().sum() - conf_mat.diagonal()


def accuracy(conf_mat):
    return conf_mat.diagonal().sum() / conf_mat.sum().sum()


def tpr(conf_mat):
    return true_positive(conf_mat) / support(conf_mat)


def fpr(conf_mat):
    return 1 - tpr(conf_mat)


def tnr(conf_mat):
    negatives = conf_mat.sum() - conf_mat.sum(axis=1)
    return true_negative(conf_mat) / negatives


def fnr(conf_mat):
    return 1 - tnr(conf_mat)


def ppv(conf_mat):
    tp = true_positive(conf_mat)
    sum_preds = conf_mat.sum(axis=0)
    return np.divide(
        tp, sum_preds, out=np.zeros_like(tp, dtype=float), where=sum_preds != 0
    )


def npv(conf_mat):
    return true_negative(conf_mat) / (conf_mat.sum() - conf_mat.sum(axis=0))


def f1(conf_mat):
    num = 2 * ppv(conf_mat) * tpr(conf_mat)
    denom = ppv(conf_mat) + tpr(conf_mat)
    return np.divide(num, denom, out=np.zeros_like(num, dtype=float), where=denom != 0)


def informedness(conf_mat):
    return tpr(conf_mat) + tnr(conf_mat) - 1


def markedness(conf_mat):
    return ppv(conf_mat) + npv(conf_mat) - 1


def mcc(conf_mat):
    c = true_positive(conf_mat).sum()
    s = support(conf_mat).sum()
    t = conf_mat.sum(axis=1)
    p = conf_mat.sum(axis=0)
    denom1 = np.sqrt(s**2 - p.dot(p))
    denom2 = np.sqrt(s**2 - t.dot(t))
    if c == s:
        return 1.0
    elif denom1 == 0 or denom2 == 0:
        return 0
    else:
        return (c * s - t.dot(p)) / (denom1 * denom2)


def macro_average(values):
    return values.mean()


def micro_average(values, conf_mat):
    sup = support(conf_mat)
    prop = sup / sup.sum()
    return (values * prop).sum()


def false_alarm_costs(y, y_pred, label_dict, cvss_scores):
    # Consider the cost as
    # IF considering only normal -> attack as false alarm
    # false_alarms = np.array(y != y_pred) * np.array(y == label_dict["normal"])
    # If considering everything wrongly classified in a specific attack as false alarm
    false_alarms = np.array(y != y_pred) * np.array(y_pred != label_dict["normal"])
    cve_means = {
        c: pd.DataFrame(
            cvss_scores[y == c].replace(-1, np.nan).dropna(axis=0).to_list()
        )
        .mean()
        .mean()
        for c in label_dict.values()
        if c != label_dict["normal"]
    }
    # reverse_ld = {v: k for k, v in label_dict.items()}
    # logger.info(
    #     "FA {}".format(
    #         np.array(
    #             [
    #                 [
    #                     reverse_ld[c],
    #                     (np.array(y != 0)).sum(),
    #                     (np.array(y == c)).sum(),
    #                     (np.array(y_pred == c)).sum(),
    #                     (np.array(y_pred == c) * np.array(y == c)).sum(),
    #                     (np.array(false_alarms * y_pred == c)).sum(),
    #                     (np.array(false_alarms * y_pred == c) * cve_means[c]).sum(),
    #                     ((np.array(y != c) * np.array(y != 0)).sum() * 10),
    #                     (np.array(false_alarms * y_pred == c) * cve_means[c]).sum()
    #                     / ((np.array(y != c) * np.array(y != 0)).sum() * 10),
    #                 ]
    #                 if not np.isnan(cve_means[c])
    #                 else 0
    #                 for c in label_dict.values()
    #                 if c != label_dict["normal"]
    #             ]
    #         )
    #     )
    # )
    # return np.array(
    #     [
    #         (np.array(false_alarms * y_pred == c) * cve_means[c]).sum()
    #         / (np.array(y != c).sum() * 10)
    #         if not np.isnan(cve_means[c])
    #         else 0
    #         for c in label_dict.values()
    #         if c != label_dict["normal"]
    #     ]
    # )
    # means_cve = (
    #     pd.DataFrame(cvss_scores.replace(-1, np.nan).dropna(axis=0).to_list())
    #     .mean()
    #     .mean()
    # )
    # for k in cve_means:
    #     if k != label_dict["normal"] and np.isnan(cve_means[k]):
    #         cve_means[k] = means_cve

    # res = np.array(
    #     [
    #         sum(
    #             [
    #                 (
    #                     np.array(false_alarms * y_pred == cprime) * cve_means[cprime]
    #                 ).sum()
    #                 / (np.array(y != cprime).sum() * 10)
    #                 for cprime in label_dict.values()
    #                 if cprime != c and cprime != label_dict["normal"]
    #             ]
    #         )
    #         for c in label_dict.values()
    #     ]
    # )
    # logger.info(res)
    # return res[1:]
    means_cve = (
        pd.DataFrame(cvss_scores.replace(-1, np.nan).dropna(axis=0).to_list())
        .mean()
        .mean()
    )
    for k in cve_means:
        if k != label_dict["normal"] and np.isnan(cve_means[k]):
            cve_means[k] = means_cve
    res = []
    for c in label_dict.values():
        if not c == label_dict["normal"]:
            if np.array(y_pred == c).sum() == 0:
                res.append(cve_means[c] / 10)
            else:
                res.append(
                    (np.array(false_alarms * y_pred == c) * cve_means[c]).sum()
                    / (np.array(y_pred == c).sum() * 10)
                )
    # logger.info(res)
    return np.array(res)


def miss_attack_costs(y, y_pred, label_dict, cvss_scores):
    missed_attacks = np.array(y != y_pred)  # * np.array(y_pred == label_dict["normal"])
    has_cve = {
        c: pd.DataFrame(
            cvss_scores[missed_attacks * (y == c)]
            .replace(-1, np.nan)
            .dropna(axis=0)
            .to_list()
        )
        for c in label_dict.values()
        if c != label_dict["normal"]
    }
    # logger.info(
    #     [
    #         has_cve[c].mean().mean()
    #         for c in label_dict.values()
    #         if c != label_dict["normal"]
    #     ]
    # )
    supp = {
        c: pd.DataFrame(
            cvss_scores[y == c].replace(-1, np.nan).dropna(axis=0).to_list()
        )
        .count()
        .sum()
        * 10
        for c in label_dict.values()
        if c != label_dict["normal"]
    }
    # logger.info(supp)
    # logger.info(
    #     "Miss {}".format(
    #         np.array(
    #             [
    #                 val.sum().sum() / supp[c] if supp[c] > 0 else 0
    #                 for c, val in has_cve.items()
    #             ]
    #         )
    #     )
    # )
    # return np.array(
    #     [
    #         val.sum().sum() / supp[c] if supp[c] > 0 else 0
    #         for c, val in has_cve.items()
    #     ]
    # )
    return np.array(
        [val.sum().sum() / supp[c] if supp[c] > 0 else 0 for c, val in has_cve.items()]
    )


def attack_costs(y, y_pred, label_dict, cvss_scores):
    return (
        1
        - false_alarm_costs(y, y_pred, label_dict, cvss_scores)
        - miss_attack_costs(y, y_pred, label_dict, cvss_scores)
    )
