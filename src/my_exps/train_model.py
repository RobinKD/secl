"""Train stage for dvc experiments."""

import json
import logging
import pickle
from pathlib import Path

import dvc.api
import numpy as np
from aim import Run
from aim.sdk.base_run import MissingRunError
from make_aim import make_aim

logging_format = "%(asctime)s - %(levelname)s - Model training: %(message)s"
logger = logging.getLogger("train_model")
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(logging_format))
logger.addHandler(handler)
logger.setLevel(10)


def train_model(params: dict) -> None:
    """Train the current model.

    Load train dataset, untrained model, and train the model.
    """
    dataset_name = params["dataset"]["name"]

    with Path(f"processed_data/{dataset_name}/dataset_info.json").open("r") as f:
        dataset_info = json.load(f)
    with Path(f"processed_data/{dataset_name}/train_data.npy").open("rb") as f:
        train_data = np.load(f)
    with Path(f"processed_data/{dataset_name}/train_labels.npy").open("rb") as f:
        train_labels = np.load(f)

    with Path("untrained_model.pkl").open("rb") as f:
        model = pickle.load(f)

    train_cvss = None
    if params["dataset"]["parameters"]["with_cve"]:
        with Path(f"processed_data/{dataset_name}/train_cvss.npy").open("r") as f:
            train_cvss = json.load(f)

    try:
        with Path("current_aim_run").open("r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
    except MissingRunError:
        make_aim(params)
        with Path("current_aim_run").open("r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
    logger.info("%s", f"Current run hash: {aim_runner.hash}")

    Path("save_experiments/train").mkdir(exist_ok=True)
    if params["model"]["type"] in ["DeepLearner", "Odin", "SupConLearner"]:
        model.set_ckpt_path("save_experiments/trained_model")
    if not train_cvss:
        model.train(
            x=train_data,
            y=train_labels,
            label_dict=dataset_info["train_labels_dict"],
            aim_runner=aim_runner,
        )
    else:
        model.train(
            x=train_data,
            y=train_labels,
            label_dict=dataset_info["train_labels_dict"],
            aim_runner=aim_runner,
            train_cvss=train_cvss,
        )

    Path("save_experiments/trained_model").mkdir(exist_ok=True)
    model.save_model("save_experiments/trained_model")


if __name__ == "__main__":
    params = dvc.api.params_show(stages="train_model")

    train_model(params)
