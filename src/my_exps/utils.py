import json
import os
import pickle
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go
import seaborn as sns
import torch
from cycler import cycler

# TODO Finish doc


def keep_part_data(data, labels, random_seed, proportion=1, min_instances_class=10):
    """
    Keep only a proportion of the shuffled dataset, for faster experiments

    :param proportion: float
    proportion of the dataset kept
    :param min_instances_class: int
    min number of instances per class
    """
    rng = np.random.default_rng(seed=random_seed)
    shuffle = rng.permutation(len(labels))
    data = data[shuffle]
    labels = labels[shuffle]
    if proportion < 1 and proportion > 0:
        instances_perclass = {
            label: np.sum(labels == label) for label in np.unique(labels)
        }
        instances_tokeep = {
            label: max(min(min_instances_class, instances), int(instances * proportion))
            for label, instances in instances_perclass.items()
        }
        new_data = []
        new_labels = []
        sum_instances = sum(instances_tokeep.values())
        for sample, label in zip(data, labels):
            if instances_tokeep[label] > 0:
                new_data.append(sample)
                new_labels.append(label)
                instances_tokeep[label] -= 1
                sum_instances -= 1
            if sum_instances == 0:
                break
        new_data = np.array(new_data)
        new_labels = np.array(new_labels)
        return new_data, new_labels
    else:
        print("Incorrect proportion value, keeping all data...")
        return data, labels


def get_perclass_log(pc_val, label_dict):
    """
    TODO Redo doc
    Returns the values per class with numeric labels replaced by their names
    """
    reverse_dict = {i: label for label, i in label_dict.items()}
    l = {}
    for v, i in zip(pc_val, reverse_dict.keys()):
        l[reverse_dict[i]] = v
    return l


def get_loss_line_coeffs(losses):
    """
    Returns the slope of the line representing the losses' evolution

    :param losses: array_like of floats
    :returns: float
    """
    x = np.arange(len(losses))
    mean_x = np.sum(x) / len(losses)
    mean_losses = np.sum(losses) / len(losses)
    return np.sum((x - mean_x) * (losses - mean_losses)) / np.sum((x - mean_x) ** 2)


def get_class_pred(preds):
    """
    Get predicted labels from a neural network output,
    i.e. get index of the highest value

    :param preds: array-like of array-like of floats, values at the last layer
    :returns: array-like of ints
    """
    preds_labels = torch.log_softmax(preds, dim=1)
    _, preds_labels = torch.max(preds_labels, dim=1)
    return preds_labels


def get_acc(preds_classes, labels):
    """
    Computes accuracy of a neural network predicted labels

    :param preds_classes: array-like of ints, classes predicted
    :param labels: array-like of ints, truth labels
    :returns: int, the percentage of correct predictions
    """
    accuracy = (preds_classes == labels).sum().item() / labels.shape[0] * 100.0
    return accuracy


def save_figure(path, high_res):
    if high_res:
        plt.savefig(path + ".svg", format="svg")
    else:
        plt.savefig(path + ".png")


def generic_plot(
    steps,
    data,
    classes,
    path_save,
    title,
    xlabel,
    ylabel,
    ylim,
    has_title=True,
    has_legend=True,
    high_res=False,
):
    fig = plt.figure(figsize=(12, 8))
    plt.gca().set_prop_cycle(
        cycler(
            "color",
            [plt.cm.gist_ncar(i) for i in np.linspace(0, 0.9, max(len(classes), 15))],
        )
    )
    # print([plt.cm.gist_ncar(i) for i in np.linspace(0, 0.9, max(len(classes), 15))])
    for i, _ in enumerate(list(classes)):
        plt.plot(steps, data[i])
    if has_legend:
        plt.legend(
            classes,
            bbox_to_anchor=(1.02, 1),
            loc="upper left",
            borderaxespad=0,
            fontsize="xx-large",
        )
    else:
        plt.legend().set_visible(False)
    if has_title:
        plt.title(title, fontsize=20, pad=10)
    plt.xlabel(xlabel, fontsize="xx-large")
    plt.ylabel(ylabel, fontsize="xx-large", labelpad=10)
    plt.xticks(fontsize="xx-large")
    plt.yticks(fontsize="xx-large")
    plt.ylim(ylim)
    plt.tight_layout()
    save_figure(path_save, high_res)
    plt.close(fig)


def perf_plot(epochs, data, path_save, title, **kwargs):
    fig = plt.figure(figsize=(15, 10))
    plt.plot(epochs, data)
    bests = max(data), epochs[np.argmax(data)]
    if title == "Loss":
        bests = min(data), epochs[np.argmin(data)]
    plt.title("{0}\nBest {0} = {1:.4f} (epoch {2})".format(title, bests[0], bests[1]))
    plt.xlabel("Epochs")
    plt.tight_layout()
    save_figure(path_save, kwargs.get("high_res", False))
    plt.close(fig)


def perclass_perf_plot(
    epochs, data, path_save, title, macro_vals, micro_vals, **kwargs
):
    fig = plt.figure(figsize=(15, 10))
    plt.gca().set_prop_cycle(
        cycler(
            "color",
            [plt.cm.nipy_spectral(i) for i in np.linspace(0, 0.9, len(data[0].keys()))],
        )
    )
    multip = 100
    bests = (
        max(macro_vals),
        epochs[np.argmax(macro_vals)],
        max(micro_vals),
        epochs[np.argmax(micro_vals)],
    )
    if title in ["FalseAlarmCost", "MissCost", "AttackCost"]:
        multip = 1
        bests = (
            min(macro_vals),
            epochs[np.argmin(macro_vals)],
            min(micro_vals),
            epochs[np.argmin(micro_vals)],
        )
    for label in list(data[0].keys()):
        plt.plot(epochs, [data[i][label] * multip for i in range(len(data))])
    plt.legend(
        data[0].keys(),
        bbox_to_anchor=(1.02, 1),
        loc="upper left",
        borderaxespad=0,
    )
    plt.rc("legend", fontsize="medium")
    plt.title(
        "{0}\nBest macro {0} = {1:.4f} (epoch {2})\nBest micro {0} = {3:.4f} (epoch {4})".format(
            title, bests[0], bests[1], bests[2], bests[3]
        )
    )
    plt.xlabel("Epochs")
    plt.tight_layout()
    save_figure(path_save, kwargs.get("high_res", False))
    plt.close(fig)


def save_cfm(accs, label_dict, fmt=".2f", figure_title=None):
    cfm_labels = label_dict.keys()
    fig_size = (6 * np.sqrt(len(cfm_labels)), 5 * np.sqrt(len(cfm_labels)))
    plt.figure(figsize=fig_size)
    if len(cfm_labels) == 2:
        fmt = ".3f"
    cfm = sns.heatmap(
        accs,
        annot=True,
        fmt=fmt,
        annot_kws={"fontsize": 75 / np.sqrt(len(cfm_labels))},
        linewidths=0.1,
        cmap=sns.color_palette("Blues", as_cmap=True),
    )
    cfm.set_xticks(np.arange(len(cfm_labels)) + 0.5)
    cfm.set_yticks(np.arange(len(cfm_labels)) + 0.5)
    cfm.set_xticklabels(
        cfm_labels, rotation=45, ha="right", rotation_mode="anchor", fontsize="x-large"
    )
    cfm.set_yticklabels(cfm_labels, rotation="horizontal", fontsize="x-large")
    cfm.set_ylabel("Ground truth", fontsize="xx-large", fontweight="bold")
    cfm.set_xlabel("Predicted", fontsize="xx-large", fontweight="bold")
    fig_title = "" if figure_title is None else figure_title + "\n"
    cfm.set_title(
        "{}Confusion Matrix".format(fig_title),
        fontsize="xx-large",
        fontweight="bold",
        pad=15,
    )
    plt.tight_layout()
    plt.close(cfm.figure)
    return cfm.figure


def save_hist(macro_val, micro_val, pc_val, name, figure_title=None):
    hist_labels = pc_val.keys()
    fig = plt.figure(
        figsize=(5 * np.sqrt(len(hist_labels)), 2 * np.sqrt(len(hist_labels)))
    )
    plt.bar(hist_labels, pc_val.values())
    plt.xticks(
        np.arange(len(hist_labels)),
        hist_labels,
        rotation=45,
        ha="right",
        rotation_mode="anchor",
    )
    plt.xlabel("Class", fontweight="bold")
    fig_title = "" if figure_title is None else figure_title + "\n"
    plt.title(
        "{0}Macro {1} = {2:.6f}\nMicro {1} = {3:.6f}".format(
            fig_title, name, macro_val, micro_val
        )
    )
    plt.ylim()
    plt.tight_layout()
    plt.close(fig)
    return fig


def plot_fac_vs_mc(dataset_res, **kwargs):
    fig = plt.figure(figsize=(24, 12))
    ftsize = 24
    marker = ["o", "+", "*", "x"]
    i = 0
    for dataset in dataset_res:
        colorcycle = cycler(
            color=[
                plt.cm.nipy_spectral(i)
                for i in np.linspace(0, 0.9, len(dataset_res[dataset].keys()))
            ]
        )
        plt.gca().set_prop_cycle(colorcycle)
        for model in dataset_res[dataset]:
            values = dataset_res[dataset][model]["values"]
            plt.scatter(
                values[0],
                values[1],
                label="{}: {}".format(dataset, model),
                marker=marker[i],
                s=ftsize * 8,
            )
        i += 1 % len(marker)

    plt.xlabel("FAC", fontweight="bold", fontsize=ftsize)
    plt.xticks(fontsize=ftsize)
    plt.xlim(-0.1, 5)
    plt.ylabel("MC", fontweight="bold", fontsize=ftsize)
    plt.yticks(fontsize=ftsize)
    handles, labels = plt.gca().get_legend_handles_labels()
    new_handles_color = []
    new_handles_marker = []
    new_labels_color = []
    new_labels_marker = []
    for h, l in zip(handles, labels):
        splitted_l = l.split(":")
        splitted_l[1] = splitted_l[1].strip(" ")
        if splitted_l[0] not in new_labels_marker:
            new_handles_marker.append(h)
            new_labels_marker.append(splitted_l[0])
        if splitted_l[1] not in new_labels_color:
            new_handles_color.append(h)
            new_labels_color.append(splitted_l[1])

    new_handles = new_handles_color + new_handles_marker
    new_labels = new_labels_color + new_labels_marker
    plt.legend(
        new_handles,
        new_labels,
        bbox_to_anchor=(1.02, 1),
        loc="upper left",
        borderaxespad=0,
        prop={"size": ftsize},
    )
    plt.title("FAC vs MC", fontweight="bold", fontsize=ftsize * 1.25)
    plt.tight_layout()
    save_figure("./save/fac_vs_mc", kwargs.get("high_res", False))
    plt.close(fig)


def plot_viz(embed, labels, labels_dict, **kwargs):
    colors = [
        plt.cm.nipy_spectral(i) for i in np.linspace(0, 0.9, len(labels_dict.keys()))
    ]
    colors = [f"rgba({r*255},{g*255},{b*255},{a})" for (r, g, b, a) in colors]
    class_names = list(labels_dict.keys())
    fig = go.Figure()
    for i in range(len(labels_dict.values())):
        idx = labels == list(labels_dict.values())[i]
        fig.add_trace(
            go.Scatter(
                x=embed[idx, 0],
                y=embed[idx, 1],
                mode="markers",
                marker_color=colors[i],
                name=class_names[i],
            )
        )

    fig.update_layout(
        title_text=kwargs["algo"] + " representation",
        title_x=0.5,
        autosize=True,
    )
    # fig = px.scatter(
    #     data_frame=embed,
    #     color=[colors[label] for label in labels],
    #     title=kwargs["algo"] + " representation",
    # )
    # fig = plt.figure(figsize=(15, 10))
    # plt.gca().set_prop_cycle(
    #     cycler(
    #         "color",
    #         [
    #             plt.cm.nipy_spectral(i)
    #             for i in np.linspace(0, 0.9, len(labels_dict.keys()))
    #         ],
    #     )
    # )
    # for i in range(len(labels_dict.values())):
    #     idx = labels == list(labels_dict.values())[i]
    #     plt.plot(embed[idx, 0], embed[idx, 1], ".", markersize=10)
    # plt.legend(
    #     labels_dict.keys(),
    #     bbox_to_anchor=(1.02, 1),
    #     loc="upper left",
    #     borderaxespad=0,
    # )
    # plt.rc("legend", fontsize="medium")
    # plt.title(kwargs["algo"] + " representation")
    # plt.tight_layout()
    # plt.close(fig)
    return fig


def redo_viz(path, **kwargs):
    if "tsne" in path or "umap" in path:
        path = os.path.splitext(path)[0]
        if "_preds" in path:
            path = path.strip("_preds")
        with open(path + ".data", "rb") as f:
            saved = pickle.load(f)
            algo = "tsne" if Path(path).stem[:4] == "tsne" else "umap"
            plot_viz(
                saved["embed"],
                saved["labels"],
                path,
                saved["labels_dict"],
                algo=algo,
                **kwargs,
            )
            if "preds" in saved:
                plot_viz(
                    saved["embed"],
                    saved["preds"],
                    path + "_preds",
                    saved["labels_dict"],
                    algo=algo,
                    **kwargs,
                )
    else:
        path_dir = "/".join(path.split("/")[:-1])
        is_cfm = "confusion_matrix" in path
        file_name = Path(path).stem
        categories = file_name.split("_")
        try:
            if "DeepNN" in path_dir:
                if is_cfm:
                    epoch = categories[-1][5:]
                    epoch = int(epoch)
                    with open(path_dir + "/log_test", "rb") as f:
                        ds = pickle.load(f)
                    epochs = {e: i for i, e in enumerate([d["Epoch"] for d in ds])}
                    is_unskew = "unskew" in file_name
                    cfm_name = "normalized_cm" if not is_unskew else "unskewed_cm"
                    cfm = ds[epochs[epoch]][cfm_name]
                    save_cfm(
                        cfm,
                        ds[0]["labels_dict"],
                        path_dir,
                        epoch,
                        suffix_name="" if not is_unskew else "unskew",
                        **kwargs,
                    )
                else:
                    train_test = categories[0]
                    metric = categories[-1]
                    with open(path_dir + "/log_" + train_test, "rb") as f:
                        ds = pickle.load(f)
                        epochs = [d["Epoch"] for d in ds if d["Epoch"] != 0]
                        vals = [d[metric] for d in ds if d["Epoch"] != 0]
                        if metric in ["Accuracy", "MCC", "Loss"]:
                            perf_plot(
                                epochs,
                                vals,
                                "{}/{}_{}".format(path_dir, train_test, metric),
                                metric,
                                **kwargs,
                            )
                        elif metric not in ["Support", "TP", "TN"]:
                            macro_vals = [
                                d["macro_" + metric] for d in ds if d["Epoch"] != 0
                            ]
                            micro_vals = [
                                d["micro_" + metric] for d in ds if d["Epoch"] != 0
                            ]
                            perclass_perf_plot(
                                epochs,
                                vals,
                                # label_dict,
                                "{}/{}_PerClass_{}".format(
                                    path_dir, train_test, metric
                                ),
                                metric,
                                macro_vals,
                                micro_vals,
                                **kwargs,
                            )
            else:
                with open(path_dir + "/log_test", "rb") as f:
                    ds = pickle.load(f)
                if is_cfm:
                    is_unskew = "unskew" in file_name
                    cfm_name = "normalized_cm" if not is_unskew else "unskewed_cm"
                    cfm = ds[cfm_name]
                    save_cfm(
                        cfm,
                        ds["labels_dict"],
                        path_dir,
                        suffix_name="" if not is_unskew else "unskew",
                        **kwargs,
                    )
                else:
                    metric = categories[-1]
                    if metric not in ["Support", "Accuracy", "MCC", "TP", "TN"]:
                        save_hist(
                            ds["macro_" + metric],
                            ds["micro_" + metric],
                            ds[metric],
                            path_dir,
                            metric,
                            **kwargs,
                        )

        except Exception as e:
            print("Exception {} caught in redo viz".format(e))


class MyJsonEncoder(json.JSONEncoder):

    """JSON Encoder to cast to correct type."""

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super().default(obj)
